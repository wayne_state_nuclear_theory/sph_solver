# SPH_solver

Smoothed particle hydrodynamics (SPH) uses a mesh-free particle scheme to solve hydrodynamic equations.

This is a code repository aims to use the SPH techniques to build a PDE solver for general partial differential equations.

## Compilation

This program is written in C++ with the C++11 standard. It can be compiled with CMake. 

## Organization

All the source files for the code, as well as the unit tests, are in the directory `src/`. A summarizing report is in the main directory, titled "SPH_Report.pdf". All the other directories hold various data files and python notebooks for different tests executed with the code.

`1d_diffusion_test/` test the implementation of our SPH scheme to the diffusion of the field in one dimension when initialized to a delta function.

`2d_diffusion_test/` tests the implementation of our SPH scheme to the diffusion of the field in two dimension when initialized to a delta function. 

`causal_diff_test/` tests the implementation of our SPH scheme to the causal-diffusion equation as outlined in Section 8 of the report.

`h_test/` tests the required spacing h needed to simulate a constant field correctly.

`matrix_source_test/` tests the implementation of our SPH scheme to the evolution of the system where the source in the differential equation is given by a matrix that leads to coupling between the 0th and 2nd component of the field vector. 

`radial_expansion_test/` tests the implementation of our SPH scheme to solve the radial expansion of a source field.

`relaxation_test/` tests the implementation of our SPH scheme to a simple relaxation equation that leads to the exponential decay of the initial field.

`shockwave_test/` tests the implementaiton of our SPH scheme to the classic shockwave problem, where we test the evolution of a shockwave as it travels through space.

## Source Files
`constant.h` defines the error constants. `datastruct.h` defines a datastructure myreal which can be switched from double to float. 

`fourvec.h` and `fourvec.cpp` defined a class fourvec which carrys an array of size 4 with methods that treat this as a fourvector in relativistic physics. 

`Random.h` and `Random.cpp` define a class Random to generate random numbers.

`SPH_Particle.h` and `SPH_Particle.cpp` define a class SPH_Particle that holds the information of a given SPH_Particle, such as its position, velocity, and mass. It also keeps track of a list of neighboring particles. It has methods to access and change all of these variables, as well as virtual methods to calculate the smoothing function W and its derivatives at different values of x. 

`SPH_Particle_Gaussian.h` and `SPH_Particle_Gaussian.cpp` define a derived class SPH_Particle_Gaussian from SPH_Particle, which specifies the smoothing function W to be a Gaussian and also includes a constant kappa that determines how to restrict the support of the Gaussian (that is, at how many standard deviations to cut off the Gaussian function).

`SPH_Cell.h` and `SPH_Cell.cpp` define a class SPH_Cell that holds the list of SPH_Particle's linked to a specific cell in a given mesh, as well as methods to access and change this list of linked particles. 

`SPH_Mesh.h` and `SPH_Mesh.cpp` define a class SPH_Mesh that holds the information of a mesh, including its dimensions and all the cells within it. It also includes a method to update the linked lists of each SPH_Cell it includes. 

`SPH_Driver.h` and `SPH_Driver.cpp` define a class SPH_Driver which is comprised of a list of SPH_Particles and an SPH_Mesh. It has an accessor function to the particle list. It has various other methods including methods to initialize the particles list, to generate a mesh, to update neighboring particle lists, to update position and velocity of particles, to evolve the system in some time step t, and to calculate source terms in the underlying differential equation which drives the system. There are also 3 different methods to solve the differential equation, which are Euler's method, generalized RK-2, and RK-4. There is finally a method that solves the given ODE between an initial and final time given a certain time step. 

`main.cpp` simply creates a driver, generates the initial particles, and then solves the ODE over some period of time. 

There are unit tests for all of the classes that were created to test the methods.  