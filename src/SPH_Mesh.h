//  file: SPH_Mesh.h
//
//  Header file for the SPH_Mesh C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      06/18/19  Original header file
//
//  Notes:
//
//***************************************************************************

// The ifndef/define macro ensures that the header is only included once
#ifndef SPH_MESH_H
#define SPH_MESH_H

// include files
#include "SPH_Cell.h"

class SPH_Mesh{
	private:
		myreal size_x_;
		myreal size_y_;
		myreal size_z_;
		myreal dx_;
		myreal dy_;
		myreal dz_;
		int Nx_;
		int Ny_;
		int Nz_;
		std::vector<SPH_Cell> mesh_;

	public:
		SPH_Mesh(const myreal size_x, const myreal size_y, const myreal size_z,
			const myreal dx, const myreal dy, const myreal dz); // Constructor
		SPH_Mesh(); // Default Constructor
		~SPH_Mesh() {} // Destructor

		// accessor functions
		int get_Nx() {return(Nx_);}
		int get_Ny() {return(Ny_);}
		int get_Nz() {return(Nz_);}
		SPH_Cell& get_cell(const int idx_x, const int idx_y, const int idx_z);

		void update_linked_list(
			std::vector<std::shared_ptr<SPH_Particle>> particle_list);
};

#endif  // SPH_MESH_H