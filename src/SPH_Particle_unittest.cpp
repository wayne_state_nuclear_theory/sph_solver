#include "doctest.h"
#include "SPH_Particle.h"
#include <cmath>

using doctest::Approx;

TEST_CASE("Check get_mass and set_mass") {
    // initialize a particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;

    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // get particle mass and check it
    auto check_v = testparticle.get_mass();
    CHECK(check_v == Approx(init_mass));

    // set particle mass, get particle mass again and check it
    myreal new_mass = 2.1;
    testparticle.set_mass(new_mass);
    check_v = testparticle.get_mass();
    CHECK(check_v == Approx(new_mass));
}

TEST_CASE("Check get_rho and set_rho") {
    // initialize a particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // get particle rho and check it
    auto check_v = testparticle.get_rho();
    CHECK(check_v == Approx(init_rho));

    // set particle rho, get particle rho again and check it
    myreal new_rho = 2.2;
    testparticle.set_rho(new_rho);
    check_v = testparticle.get_rho();
    CHECK(check_v == Approx(new_rho));
}

TEST_CASE("Check get_weight and set_weight") {
    // initialize a particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // get particle weight vector and check it
    auto check_v = testparticle.get_weight();
    CHECK(check_v[0] == Approx(init_weight[0]));
    CHECK(check_v[1] == Approx(init_weight[1]));

    // set particle weight vector, get particle weight vector again and check it
    std::vector<myreal> new_weight = {2.3, 2.4};
    testparticle.set_weight(new_weight);
    check_v = testparticle.get_weight();
    CHECK(check_v[0] == Approx(new_weight[0]));
    CHECK(check_v[1] == Approx(new_weight[1]));
}

TEST_CASE("Check get_position and set_position") {
    // initialize a particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // get particle position and check it
    auto check_v = testparticle.get_position();
    CHECK(check_v[0] == Approx(init_x[0]));
    CHECK(check_v[1] == Approx(init_x[1]));
    CHECK(check_v[2] == Approx(init_x[2]));
    CHECK(check_v[3] == Approx(init_x[3]));

    // set particle position, get particle position again and check it
    fourvec new_x(5.1, 6.1, 7.1, 8.1);
    testparticle.set_position(new_x);
    check_v = testparticle.get_position();
    CHECK(check_v[0] == Approx(new_x[0]));
    CHECK(check_v[1] == Approx(new_x[1]));
    CHECK(check_v[2] == Approx(new_x[2]));
    CHECK(check_v[3] == Approx(new_x[3]));
}

TEST_CASE("Check get_velocity and set_velocity") {
    // initialize a particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // get particle velocity and check it
    auto check_v = testparticle.get_velocity();
    CHECK(check_v[0] == Approx(init_u[0]));
    CHECK(check_v[1] == Approx(init_u[1]));
    CHECK(check_v[2] == Approx(init_u[2]));
    CHECK(check_v[3] == Approx(init_u[3]));

    // set particle velocity, get particle velocity again and check it
    fourvec new_u(1.5, std::sqrt(2.)/2., std::sqrt(2.)/2., 1./2.);
    testparticle.set_velocity(new_u);
    check_v = testparticle.get_velocity();
    CHECK(check_v[0] == Approx(new_u[0]));
    CHECK(check_v[1] == Approx(new_u[1]));
    CHECK(check_v[2] == Approx(new_u[2]));
    CHECK(check_v[3] == Approx(new_u[3]));
}

TEST_CASE("Check get_h, set_h_int, and set_h") {
    // initiailize particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // get particle h and check it
    auto check_v = testparticle.get_h();
    CHECK(check_v == Approx(init_h));

    // set particle h, check particle h again and check it
    myreal new_h = 2.;
    testparticle.set_h(new_h);
    check_v = testparticle.get_h();
    CHECK(check_v == Approx(new_h));

    // set particle h, check particle h again and check it
    new_h = 3.;
    testparticle.set_h_int(new_h);
    check_v = testparticle.get_h();
    CHECK(check_v == Approx(new_h));
}

TEST_CASE("Check get_dim, set_dim_int, and set_dim") {
    // initiailize particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // get particle dim and check it
    auto check_v = testparticle.get_dim();
    CHECK(check_v == init_dim);

    // set particle dim, check particle dim again and check it
    int new_dim = 2;
    testparticle.set_dim(new_dim);
    check_v = testparticle.get_dim();
    CHECK(check_v == new_dim);

    // set particle dim, check particle dim again and check it
    new_dim = 1;
    testparticle.set_dim_int(new_dim);
    check_v = testparticle.get_dim();
    CHECK(check_v == new_dim);
}

TEST_CASE("Check neighbor_list and its functions") {
    // initialize first particle
    myreal init_mass1 = 1.1;
    myreal init_rho1 = 1.2;
    fourvec init_u1(2.,1.,1.,1.);
    fourvec init_x1(5.,6.,7.,8.);
    std::vector<myreal> init_weight1 = {1.3, 1.4};
    myreal init_h1 = 10.;
    int init_dim = 3;

    SPH_Particle testparticle1(init_mass1, init_rho1, init_weight1, init_x1, 
        init_u1, init_h1, init_dim);

    // get particle neighbor list and check that it's empty
    auto check_v = testparticle1.get_neighbor_list();
    CHECK(check_v.size() == 0);

    // initialize neighbor particle with a shared pointer
    myreal init_mass2 = 2.1;
    myreal init_rho2 = 2.2;
    fourvec init_u2(1.,0.,0.,0.);
    fourvec init_x2(5.1,6.1,7.1,8.1);
    std::vector<myreal> init_weight2 = {2.3, 2.4};
    myreal init_h2 = 1.;

    std::shared_ptr<SPH_Particle> temp_particle_ptr (
        new SPH_Particle(init_mass2, init_rho2, init_weight2, init_x2, init_u2, 
            init_h2, init_dim));

    // add testparticle2 to testparticle1's neighbor list
    testparticle1.add_to_neighbor_list(temp_particle_ptr);
    // get testparticle1 neighbor_list and check that it has size 1
    check_v = testparticle1.get_neighbor_list();
    CHECK(check_v.size() == 1);

    // check all the private variable from testparticle1's neighbor
    auto check_v2 = check_v[0].lock()->get_mass();
    CHECK(check_v2 == Approx(init_mass2));

    check_v2 = check_v[0].lock()->get_rho();
    CHECK(check_v2 == Approx(init_rho2));

    auto check_v3 = check_v[0].lock()->get_position();
    CHECK(check_v3[0] == Approx(init_x2[0]));
    CHECK(check_v3[1] == Approx(init_x2[1]));
    CHECK(check_v3[2] == Approx(init_x2[2]));
    CHECK(check_v3[3] == Approx(init_x2[3]));

    check_v3 = check_v[0].lock()->get_velocity();
    CHECK(check_v3[0] == Approx(init_u2[0]));
    CHECK(check_v3[1] == Approx(init_u2[1]));
    CHECK(check_v3[2] == Approx(init_u2[2]));
    CHECK(check_v3[3] == Approx(init_u2[3]));

    check_v2 = check_v[0].lock()->get_h();
    CHECK(check_v2 == Approx(init_h2));

    check_v2 = check_v[0].lock()->get_dim();
    CHECK(check_v2 == init_dim);

    auto check_v4 = check_v[0].lock()->get_weight();
    CHECK(check_v4[0] == Approx(init_weight2[0]));
    CHECK(check_v4[1] == Approx(init_weight2[1]));

    // clear testparticle1's neighbor_list and check that it is now empty
    testparticle1.clear_neighbor_list();
    check_v = testparticle1.get_neighbor_list();
    CHECK(check_v.size() == 0);
}

TEST_CASE("Check default constructor") {
    // initialize default particle
    SPH_Particle testparticle;

    // check all the private variable for default particle
    auto check_v = testparticle.get_mass();
    CHECK(check_v == Approx(1.));

    check_v = testparticle.get_rho();
    CHECK(check_v == Approx(1.));

    auto check_v2 = testparticle.get_position();
    CHECK(check_v2[0] == Approx(0.));
    CHECK(check_v2[1] == Approx(0.));
    CHECK(check_v2[2] == Approx(0.));
    CHECK(check_v2[3] == Approx(0.));

    check_v2 = testparticle.get_velocity();
    CHECK(check_v2[0] == Approx(1.));
    CHECK(check_v2[1] == Approx(0.));
    CHECK(check_v2[2] == Approx(0.));
    CHECK(check_v2[3] == Approx(0.));

    check_v = testparticle.get_h();
    CHECK(check_v == Approx(0.1));

    check_v = testparticle.get_dim();
    CHECK(check_v == 3);

    // check that weight vector is empty
    auto check_v3 = testparticle.get_weight();
    CHECK(check_v3.size() == 0);
}

TEST_CASE("Check default smoothing functions") {
    // initialize a particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // check all default smoothing functions at position x_j, should be 0.
    fourvec x_j(-1., -1., -1., -1.);
    auto check_v = testparticle.W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dr_W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dr2_W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dx_W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dy_W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dz_W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dx2_W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dy2_W(x_j);
    CHECK(check_v == Approx(0.));

    check_v = testparticle.dz2_W(x_j);
    CHECK(check_v == Approx(0.));
}

TEST_CASE("Check check_distance") {
    // this has radius_sq_ = 1. initialized 
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 0.1;
    int init_dim = 3;
    
    SPH_Particle testparticle(init_mass, init_rho, init_weight, init_x, init_u, 
        init_h, init_dim);

    // check distance at x_j such that distance is just less than 1.
    fourvec delta_x1(0., 0.99, 0., 0.);
    fourvec x_j = init_x + delta_x1;
    auto check_v = testparticle.check_distance(x_j);
    CHECK(check_v == true);

    // check distance at x_j such that distance is just greater than 1.
    fourvec delta_x2(0., 1.01, 0., 0.);
    x_j = init_x + delta_x2;
    check_v = testparticle.check_distance(x_j);
    CHECK(check_v == false);
}
