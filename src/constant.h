//  file: constant.h
//
//  Header file for constants.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/30/19  Original header file
//
//  Notes:
//
//***************************************************************************

// The ifndef/define macro ensures that the header is only included once
#ifndef CONSTANT_H
#define CONSTANT_H

namespace Utility {
	const double abserr = 1e-15;
	const double small_eps = 1e-15;
}

#endif  // CONSTANT_H
