//  file: fourvec.h
//
//  Header file for the fourvec C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/31/19  Original header file
//
//  Notes:
//
//***************************************************************************

// include files
#include <array>
#include "datastruct.h"

class fourvec {
    private:
        std::array<myreal, 4> a_; // array of size 4

    public:
        fourvec(const myreal a0, const myreal a1, const myreal a2,
                const myreal a3); // Constructor
        fourvec() {a_={0., 0., 0., 0.};} // Default Constructor
        ~fourvec() {} // Desctructor
    
        // accessor functions
        void set_a_i(const int i, const myreal a0) {a_[i] = a0;}
        void set_a(const std::array<myreal, 4> a) {a_ = a;}
        std::array<myreal, 4> get_a() const {return a_;}
        
        // norm and scalar product functions
        myreal norm_sq() const;
        myreal norm() const;
        myreal scalarproduct(const fourvec &v2) const;
        myreal three_norm_sq() const;
        myreal three_norm() const;
        myreal three_scalarproduct(const fourvec &v2) const;
    
        // overload operators
        myreal operator[](const int i) const;
        fourvec operator +(const fourvec &v2) const;
        fourvec operator -(const fourvec &v2) const;
        fourvec operator *(const myreal c) const;
        fourvec operator /(const myreal c) const;
        fourvec & operator =(const fourvec &v2) = default;
};

