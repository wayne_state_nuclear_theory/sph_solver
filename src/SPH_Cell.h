//  file: SPH_Cell.h
//
//  Header file for the SPH_Cell C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      06/18/19  Original header file
//
//  Notes:
//
//***************************************************************************

// The ifndef/define macro ensures that the header is only included once
#ifndef SPH_CELL_H
#define SPH_CELL_H

// include files
#include "datastruct.h"
#include "SPH_Particle.h"
#include "SPH_Particle_Gaussian.h"

class SPH_Cell{
	private:
		// list of particles linked to this cell
		std::vector<std::weak_ptr<SPH_Particle>> linked_list_;

	public:
		SPH_Cell() {}; // Default Constructor
		~SPH_Cell() {} // Destructor

		// accessor functions
		void add_to_linked_list(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        void clear_linked_list () {linked_list_.clear();}

        std::vector<std::weak_ptr<SPH_Particle>> get_linked_list(
        	) const;
};

#endif  // SPH_CELL_H