#include "doctest.h"
#include "SPH_Mesh.h"
#include <cmath>

using doctest::Approx;

TEST_CASE("Check accessor functions get_Nx, get_Ny, and get_Nz") {
	// initialize testmesh
	SPH_Mesh testmesh(10., 10., 10., 0.1, 0.2, 0.5);
	// this mesh should have have Nx = 101, Ny = 51, Nz = 21
	auto check_v = testmesh.get_Nx();
	CHECK(check_v == 101);

	check_v = testmesh.get_Ny();
	CHECK(check_v == 51);

	check_v = testmesh.get_Nz();
	CHECK(check_v == 21);
}

TEST_CASE("Check update_linked_list and get_cell") {
	// initialize testmesh
	SPH_Mesh testmesh(1., 1., 1., 0.1, 0.1, 0.1);

	// initialize a list of particles for the linked_list
	// initialize first particle
    myreal init_mass1 = 1.1;
    myreal init_rho1 = 1.2;
    fourvec init_u(1.,0.,0.,0.);
    fourvec init_x1(0., 0.15, 0., 0.);
    std::vector<myreal> init_weight1 = {1.3, 1.4};
    myreal init_h = 1.;
    myreal init_dim = 3;
    std::shared_ptr<SPH_Particle> testparticle_ptr1(
    	new SPH_Particle(init_mass1, init_rho1, init_weight1, init_x1, init_u, 
            init_h, init_dim));

    // initialize second particle
    myreal init_mass2 = 2.1;
    myreal init_rho2 = 2.2;
    fourvec init_x2(0., 0., 0.15, 0.);
    std::vector<myreal> init_weight2 = {2.3, 2.4};
    std::shared_ptr<SPH_Particle> testparticle_ptr2 (
        new SPH_Particle(init_mass2, init_rho2, init_weight2, init_x2, init_u, 
            init_h, init_dim));

    // initialize third particle
    myreal init_mass3 = 3.1;
    myreal init_rho3 = 3.2;
    fourvec init_x3(0., 0., 0.16, 0.);
    std::vector<myreal> init_weight3 = {3.3, 3.4};
    std::shared_ptr<SPH_Particle> testparticle_ptr3 (
        new SPH_Particle(init_mass3, init_rho3, init_weight3, init_x3, init_u, 
            init_h, init_dim));

    // initialize fourth particle
    myreal init_mass4 = 4.1;
    myreal init_rho4 = 4.2;
    fourvec init_x4(0., 0., 0., -0.15);
    std::vector<myreal> init_weight4 = {4.3, 4.4};
    std::shared_ptr<SPH_Particle> testparticle_ptr4 (
        new SPH_Particle(init_mass4, init_rho4, init_weight4, init_x4, init_u, 
            init_h, init_dim));

    // add particle_ptrs to a list
    std::vector<std::shared_ptr<SPH_Particle>> testlist;
    testlist.push_back(testparticle_ptr1);
    testlist.push_back(testparticle_ptr2);
    testlist.push_back(testparticle_ptr3);
    testlist.push_back(testparticle_ptr4);

    // update linked list with these particles
    testmesh.update_linked_list(testlist);

    // check various cells to see that they got the correct particles
    auto check_v = testmesh.get_cell(6, 5, 5).get_linked_list();
    CHECK(check_v.size() == 1);
    CHECK(check_v[0].lock() == testparticle_ptr1);

    check_v = testmesh.get_cell(5, 6, 5).get_linked_list();
    CHECK(check_v.size() == 2);
    CHECK(check_v[0].lock() == testparticle_ptr2);
    CHECK(check_v[1].lock() == testparticle_ptr3);

    check_v = testmesh.get_cell(5, 5, 3).get_linked_list();
    CHECK(check_v.size() == 1);
    CHECK(check_v[0].lock() == testparticle_ptr4);

    // check that some neighboring cells got no particles
    check_v = testmesh.get_cell(5, 5, 5).get_linked_list();
    CHECK(check_v.size() == 0);

    check_v = testmesh.get_cell(7, 5, 5).get_linked_list();
    CHECK(check_v.size() == 0);

    check_v = testmesh.get_cell(6, 4, 5).get_linked_list();
    CHECK(check_v.size() == 0);

    check_v = testmesh.get_cell(6, 6, 5).get_linked_list();
    CHECK(check_v.size() == 0);

    check_v = testmesh.get_cell(6, 5, 4).get_linked_list();
    CHECK(check_v.size() == 0);

    check_v = testmesh.get_cell(6, 5, 6).get_linked_list();
    CHECK(check_v.size() == 0);

}