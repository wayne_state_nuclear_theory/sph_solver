#include "doctest.h"
#include "SPH_Cell.h"
#include <cmath>

using doctest::Approx;

TEST_CASE("Check linked_list and its functions") {
	// initialize test cell
	SPH_Cell testcell;

	// get linked_list_ and check that it's empty
	auto check_v = testcell.get_linked_list();
	CHECK(check_v.size() == 0);

	// initialize temp_particle_ptr to put into linked list
	myreal init_mass = 1.1;
	myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 1.;
    int init_dim = 3;
    std::shared_ptr<SPH_Particle> temp_particle_ptr (
        new SPH_Particle(init_mass, init_rho, init_weight, init_x, init_u, 
        	init_h, init_dim));

    // add temp_particle_ptr to testcell's linked list
    testcell.add_to_linked_list(temp_particle_ptr);
    // get tempcell's linked_list_ and check that it has size 1
    check_v = testcell.get_linked_list();
	CHECK(check_v.size() == 1);

	// check that the ptr in linked list and temp_particle_ptr point to the
	// same object
	CHECK(check_v[0].lock() == temp_particle_ptr);

	// clear testcell's linked_list_ and check that it is empty again
	testcell.clear_linked_list();
	check_v = testcell.get_linked_list();
	CHECK(check_v.size() == 0);
}