//  file: datastruct.h
//
//  Header file for datastructures.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/30/19  Original header file
//
//  Notes:
//
//***************************************************************************

// The ifndef/define macro ensures that the header is only included once
#ifndef DATASTRUCT_H
#define DATASTRUCT_H

// include files
#include <array>

typedef double myreal;

#endif  // DATASTRUCT_H
