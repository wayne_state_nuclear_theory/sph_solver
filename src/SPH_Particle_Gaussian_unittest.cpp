#include "doctest.h"
#include "SPH_Particle_Gaussian.h"
#include <cmath>

using doctest::Approx;

TEST_CASE("Check first constructor") {
	// initialize a Gaussian particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2; 
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    SPH_Particle_Gaussian testparticle(init_mass, init_rho, init_weight, init_x, 
        init_u, init_h, init_dim);

    // check all privatve variables for this particle
    auto check_v = testparticle.get_mass();
    CHECK(check_v == Approx(init_mass));

    check_v = testparticle.get_rho();
    CHECK(check_v == Approx(init_rho));

    auto check_v2 = testparticle.get_position();
    CHECK(check_v2[0] == Approx(init_x[0]));
    CHECK(check_v2[1] == Approx(init_x[1]));
    CHECK(check_v2[2] == Approx(init_x[2]));
    CHECK(check_v2[3] == Approx(init_x[3]));

    check_v2 = testparticle.get_velocity();
    CHECK(check_v2[0] == Approx(init_u[0]));
    CHECK(check_v2[1] == Approx(init_u[1]));
    CHECK(check_v2[2] == Approx(init_u[2]));
    CHECK(check_v2[3] == Approx(init_u[3]));

    check_v = testparticle.get_h();
    CHECK(check_v == Approx(init_h));

    check_v = testparticle.get_dim();
    CHECK(check_v == init_dim);

    auto check_v3 = testparticle.get_weight();
    CHECK(check_v3.size() == init_weight.size());
    CHECK(check_v3[0] == Approx(init_weight[0]));
    CHECK(check_v3[1] == Approx(init_weight[1]));
}

TEST_CASE("Check default constructor") {
	// initialize a default Gaussian particle 
    SPH_Particle_Gaussian testparticle;

    // check all privatve variables for this particle
    auto check_v = testparticle.get_mass();
    CHECK(check_v == Approx(1.));

    check_v = testparticle.get_rho();
    CHECK(check_v == Approx(1.));

    auto check_v2 = testparticle.get_position();
    CHECK(check_v2[0] == Approx(0.));
    CHECK(check_v2[1] == Approx(0.));
    CHECK(check_v2[2] == Approx(0.));
    CHECK(check_v2[3] == Approx(0.));

    check_v2 = testparticle.get_velocity();
    CHECK(check_v2[0] == Approx(1.));
    CHECK(check_v2[1] == Approx(0.));
    CHECK(check_v2[2] == Approx(0.));
    CHECK(check_v2[3] == Approx(0.));

    check_v = testparticle.get_h();
    CHECK(check_v == Approx(0.1));

    check_v = testparticle.get_dim();
    CHECK(check_v == 3);

    // check that weight vector is empty
    auto check_v3 = testparticle.get_weight();
    CHECK(check_v3.size() == 0);
}

TEST_CASE("Check Guassian smoothing functions") {
	// initialize a Gaussian particle
    myreal init_mass = 1.1;
    myreal init_rho = 1.2; 
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 10.;
    int init_dim = 3;
    SPH_Particle_Gaussian testparticle(init_mass, init_rho, init_weight, init_x, 
        init_u, init_h, init_dim);

    // check values of Gaussian smoothing function and derivatives at x_j with
    // values from Wolfram alpha
    fourvec x_j(-1., -1., -1., -1.);
    auto check_v = testparticle.W(x_j);
    CHECK(check_v == Approx(0.0000240693604101832019836503056189));

    check_v = testparticle.dr_W(x_j);
    CHECK(check_v == Approx(-3.35247397376515256647462578906e-6));

    check_v = testparticle.dr2_W(x_j);
    CHECK(check_v == Approx(2.26252e-7));

    check_v = testparticle.dx_W(x_j);
    CHECK(check_v == Approx(-1.68486e-6));

    check_v = testparticle.dy_W(x_j);
    CHECK(check_v == Approx(-1.92555e-6));

    check_v = testparticle.dz_W(x_j);
    CHECK(check_v == Approx(-2.16624e-6));

    check_v = testparticle.dx2_W(x_j);
    CHECK(check_v == Approx(-1.22754e-7));

    check_v = testparticle.dy2_W(x_j);
    CHECK(check_v == Approx(-8.66497e-8));

    check_v = testparticle.dz2_W(x_j);
    CHECK(check_v == Approx(-4.57318e-8));

    // change dimension of particle 
    int new_dim = 1;
    testparticle.set_dim(new_dim);
    int delta_dim = init_dim - new_dim;
    // Check that smoothing function value changes by the correct constant
    check_v = testparticle.W(x_j);
    CHECK(check_v == Approx(0.00002406936 
        * pow(init_h * init_h * 2. * M_PI, double(delta_dim)/2.)));
}

TEST_CASE("Check check_distance") {
    // for Guassian particle, kappa_ = 5. is initialized
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;  
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 0.1;  // initialize h_ = 0.1, so that radius_sq_ = (0.5)^2
    int init_dim = 3;
    SPH_Particle_Gaussian testparticle(init_mass, init_rho, init_weight, init_x, 
        init_u, init_h, init_dim);

    // check distance at x_j such that distance is just less than 5 * init_h
    fourvec delta_x1(0., 0.99 * 5 * init_h, 0., 0.);
    fourvec x_j = init_x + delta_x1;
    auto check_v = testparticle.check_distance(x_j);
    CHECK(check_v == true);

    // check distance at x_j such that distance is just greater than 5 * init_h
    fourvec delta_x2(0., 1.01 * 5 *init_h, 0., 0.);
    x_j = init_x + delta_x2;
    check_v = testparticle.check_distance(x_j);
    CHECK(check_v == false);
}

TEST_CASE("Check set_h, and that it also changes radius_sq_ and norm_const_") {
    // for Guassian particle, kappa_ = 5. is initialized 
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 0.1;  // initialize h_ = 0.1, so that radius_sq_ = (0.5)^2
    int init_dim = 3;
    SPH_Particle_Gaussian testparticle(init_mass, init_rho, init_weight, init_x, 
        init_u, init_h, init_dim);

    // check distance at x_j such that distance is just less than 5 * init_h
    fourvec delta_x1(0., 0.99 * 5 * init_h, 0., 0.);
    fourvec x_j = init_x + delta_x1;
    auto check_v = testparticle.check_distance(x_j);
    CHECK(check_v == true);

    // check that norm_const_ is correct for this h
    auto check_v2 = testparticle.get_norm_const();
    CHECK(check_v2 == 
        Approx(1./pow(init_h * init_h * 2. * M_PI, double(init_dim)/2.)));

    // reset h to be smaller, get_h and check that it is right
    myreal new_h = 0.05;
    testparticle.set_h(new_h);
    auto check_v3 = testparticle.get_h();
    CHECK(check_v3 == Approx(new_h));

    // check that radius_sq changed as well by testing distance to same
    // x_j, as now it should fail
    check_v = testparticle.check_distance(x_j);
    CHECK(check_v == false);

    // check that norm_const_ is correct for this h
    check_v2 = testparticle.get_norm_const();
    CHECK(check_v2 == 
        Approx(1./pow(new_h * new_h * 2. * M_PI, double(init_dim)/2.)));
}

TEST_CASE("Check set_dim, and that it also changes norm_const_") {
    // for Guassian particle, kappa_ = 5. is initialized 
    myreal init_mass = 1.1;
    myreal init_rho = 1.2;
    fourvec init_u(2.,1.,1.,1.);
    fourvec init_x(5.,6.,7.,8.);
    std::vector<myreal> init_weight = {1.3, 1.4};
    myreal init_h = 0.1; // initialize h_ = 0.1, 
    int init_dim = 3; // initialize dim_ = 3
    SPH_Particle_Gaussian testparticle(init_mass, init_rho, init_weight, init_x, 
        init_u, init_h, init_dim);

    // check that norm_const_ is correct for this h
    auto check_v = testparticle.get_norm_const();
    CHECK(check_v == 
        Approx(1./pow(init_h * init_h * 2. * M_PI, double(init_dim)/2.)));

    // reset dim, get_dim and check that it is right
    int new_dim = 2;
    testparticle.set_dim(new_dim);
    auto check_v2 = testparticle.get_dim();
    CHECK(check_v2 == new_dim);

    // check that norm_const_ is correct for this h
    check_v = testparticle.get_norm_const();
    CHECK(check_v == 
        Approx(1./pow(init_h * init_h * 2. * M_PI, double(new_dim)/2.)));  
}