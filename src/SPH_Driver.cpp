//  file: SPH_Driver.cpp
//
//  Definitions for the SPH_Driver C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/31/19  Original file
//
//  Notes:
//
//***************************************************************************

// include files
#include "SPH_Driver.h"  // include the header for this class
#include "constant.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <cassert>

using Utility::abserr;
using Utility::small_eps;

SPH_Driver::SPH_Driver(const int num_particle, const int type) {
	// This fucntion constructs a SPH_Driver with num_particle particles. The
	// type of particle is given by type.
	// 0 = Gaussian.
	type_ = type;
	if (type_ == 0) {
		for (int i = 0; i < num_particle; i++) {
			std::shared_ptr<SPH_Particle> temp_particle_ptr (
				new SPH_Particle_Gaussian());
			particles_ptr_list_.push_back(temp_particle_ptr);
		}
	} else {
		exit(0);
	}
}


SPH_Driver::~SPH_Driver() {
	// This function is the Deconstructor
}


std::vector<std::shared_ptr<SPH_Particle>> SPH_Driver::get_particles_ptr_list(
	) const {
	// This function returns particles_ptr_list_
	return(particles_ptr_list_);
}


void SPH_Driver::generate_init_particles() {
	// This function generates the initial particle list according to some 
	// initial scheme

	// create a random number generator for a certain boxwidth
	myreal boxwidth = 6.;
	RandomUtil::Random rand_num(0., -boxwidth/2., boxwidth/2.);
	int num = particles_ptr_list_.size();
	int sqrt_num = static_cast<int>(sqrt(num));
	int cbrt_num = static_cast<int>(cbrt(num));

	int dim = 1; // dimension of initialzed box
	int x_num = 1; // number of particles in each dimension

	if (dim == 1) {
		x_num = num;
	}
	if (dim == 2) {
		x_num = sqrt_num;
	}
	if (dim == 3) {
		x_num = cbrt_num;
	}

	// initialize to iterator over particle index
	int i = 0;
	int j = 0;
	int k = 0;
	int index = 0;

	for (auto & particle_i_ptr : particles_ptr_list_) {
		// initialize position randomly
		// myreal x = rand_num.rand_uniform();
		// myreal y = rand_num.rand_uniform();
		// myreal z = rand_num.rand_uniform();

		// initialize in 1d even spread over box
		myreal x = - boxwidth/2. 
					+ (boxwidth) * (double(i))/double(num-1);
		myreal y = 0.;
		myreal z = 0.;

		// initialize in 2d even spread over box
		// myreal x = - boxwidth/2. 
		// 			+ (boxwidth) * (double(i))/double(sqrt_num-1);
		// myreal y = - boxwidth/2. 
		// 			+ (boxwidth) * (double(j))/double(sqrt_num-1);
		// myreal z = 0.;

		// initialize in 3d even spread over box
		// myreal x = - boxwidth/2. 
		// 			+ (boxwidth) * (double(i))/double(cbrt_num-1);
		// myreal y = - boxwidth/2. 
		// 			+ (boxwidth) * (double(j))/double(cbrt_num-1);
		// myreal z = - boxwidth/2. 
		// 			+ (boxwidth) * (double(k))/double(cbrt_num-1);

		// initialize in 1d even spread over 1/10 of box
		// myreal x = - 4.*boxwidth/10. 
		// 			+ (boxwidth/10.)*(double(i))/double(num-1);
		// myreal y = 0.;
		// myreal z = 0.;

		// set particle position
		fourvec new_x(0., x, y, z);
		particle_i_ptr->set_position(new_x);

		particle_i_ptr->set_mass(1.);

		// initialize constant field 
		std::vector<myreal> new_weight (1, 2.);
		particle_i_ptr->set_weight(new_weight);

		// initialize delta function 1-d field
		// std::vector<myreal> new_weight(1, 0.);
		// myreal delta = boxwidth/(2.*x_num);
		// if((abs(x) < delta) & (abs(y) < delta) & (abs(z) < delta)){
		// 	new_weight[0] = 10.;
		// }
		// particle_i_ptr->set_weight(new_weight);

		// initialize delta function 5-d field
		// std::vector<myreal> new_weight(5, 0.);
		// myreal delta = boxwidth/(2.*x_num);
		// if((abs(x) < delta) & (abs(y) < delta) & (abs(z) < delta)){
		// 	new_weight[0] = 10.;
		// }
		// particle_i_ptr->set_weight(new_weight);

		// initialize Gaussian field
		// std::vector<myreal> new_weight;
		// myreal sigma = (boxwidth/10.)/6.;
		// myreal x0 = -3.5 * boxwidth/10.;
		// std::vector<myreal> new_weight(1, 2.*exp(-(x-x0)*(x-x0)/(sigma*sigma)));
		// particle_i_ptr->set_weight(new_weight);

		// set particle h
		particle_i_ptr->set_h(0.085);

		// set particle dim
		particle_i_ptr->set_dim(dim);

		index++;
		i = index%x_num;
		j = static_cast<int>(index/x_num)%x_num;
		k = static_cast<int>(index/(x_num*x_num));
	}

	update_velocity(); // generate velocities based on scheme in class
}


void SPH_Driver::generate_from_file(std::string SPH_particles_filename) {
	// This function generates the initial particle list from a file
	std::string text_string;
	std::ifstream SPH_particles_file(SPH_particles_filename.c_str());
	particles_ptr_list_.clear();

	// check if you can't open file
	if (!SPH_particles_file) {
		std::cout << "ERROR: Can not open SPH strings file: " 
				  << SPH_particles_filename << std::endl;
		exit(1);
	}
	getline(SPH_particles_file, text_string); // read the header
	// now read first line of data to get weight vector length
	getline(SPH_particles_file, text_string);
	if(!SPH_particles_file.eof()) {
		// temporary variables being read in
		myreal mass_temp, rho_temp, h_temp, x_temp, y_temp, z_temp, vx_temp, 
			vy_temp, vz_temp;
		std::vector<myreal> weight_temp;
		myreal weight_i_temp;
		std::shared_ptr<SPH_Particle> new_particle_ptr;

		if (type_ == 0) {
			new_particle_ptr = std::make_shared<SPH_Particle_Gaussian> ();
		}

		std::stringstream text_stream(text_string);
		text_stream >> mass_temp >> rho_temp >> h_temp >> x_temp >> y_temp 
			>> z_temp >> vx_temp >> vy_temp;

		if (!text_stream.eof()) {
			// read in last element
			text_stream >> vz_temp;
		} else {
			// the file line is too short
			std::cout << "ERROR: the format of file " << SPH_particles_filename
					  << " is wrong." << std::endl;
			exit(1);
		}
		while (!text_stream.eof()) {
			// take in the weight vector
			text_stream >> weight_i_temp;
			weight_temp.push_back(weight_i_temp);
		}
		// get size of weight vector
		int weight_size = weight_temp.size();
		// get the new fourvectors
		fourvec x_new(0., x_temp, y_temp, z_temp);
		fourvec v_new(1., vx_temp, vy_temp, vz_temp);
		// check velocities
		if (v_new.three_norm_sq()>1.) {
			// particle is faster than speed of light
			std::cout << "ERROR: velocity given is faster than speed of light. "
			          << "Given velocity: v_x = " << vx_temp 
			          << ", v_y = " << vy_temp << ", v_z = " << vz_temp << "."
			          << std::endl;
			exit(1);
		}
		// if velocity is good, divide by norm to get four velocity
		fourvec u_new = v_new / v_new.norm();
		// set the new_particle_ptr and add it to particles_ptr_list
		new_particle_ptr->set_mass(mass_temp);
		new_particle_ptr->set_rho(1.);
		new_particle_ptr->set_position(x_new);
		new_particle_ptr->set_velocity(u_new);
		new_particle_ptr->set_weight(weight_temp);
		particles_ptr_list_.push_back(new_particle_ptr);
	
		// now read rest of data
		getline(SPH_particles_file, text_string);
		while (!SPH_particles_file.eof()) {
			// make new particle ptr for the loop
			std::shared_ptr<SPH_Particle> new_particle_ptr1;
			if (type_ == 0) {
				new_particle_ptr1 = std::make_shared<SPH_Particle_Gaussian> ();
			}

			// make new text stream for the loop
			std::stringstream text_stream1(text_string);
			text_stream1 >> mass_temp >> rho_temp >> h_temp >> x_temp >> y_temp 
				>> z_temp >> vx_temp >> vy_temp;
			if (!text_stream1.eof()) {
				// read in last element
				text_stream1 >> vz_temp;
			} else {
				// the file line is too short
				std::cout << "ERROR: the format of file " 
				          << SPH_particles_filename
					 	  << " is wrong." << std::endl;
				exit(1);
			}
			for(int i = 0; i < weight_size; i++) {
				if (text_stream1.eof()) {
					std::cout << "ERROR: the format of file " 
					      << SPH_particles_filename << " is wrong." 
					      << std::endl;
					exit(1);
				}
				text_stream1 >> weight_i_temp;
				weight_temp[i] = weight_i_temp;
			}
			if (!text_stream1.eof()) {
				// the file line is too long
				std::cout << "ERROR: the format of file " 
				          << SPH_particles_filename
						  << " is wrong." << std::endl;
				exit(1);
			}
			// get the new fourvectors
			fourvec x_new(0., x_temp, y_temp, z_temp);
			fourvec v_new(1., vx_temp, vy_temp, vz_temp);
			// check velocities
			if (v_new.three_norm_sq()>1.) {
				// particles is faster than speed of light
				std::cout << "ERROR: velocity given is faster than the speed "
				          << "of light. Given velocity: v_x = " << vx_temp 
				          << ", v_y = " << vy_temp 
				          << ", v_z = " << vz_temp << "."
				          << std::endl;
				exit(1);
			}
			// if velocity is good, divide by norm to get four velocity
			fourvec u_new = v_new / v_new.norm();
	
			// set the new_particle_ptr and add it to particles_ptr_list
			new_particle_ptr1->set_mass(mass_temp);
			new_particle_ptr1->set_rho(rho_temp);
			new_particle_ptr1->set_position(x_new);
			new_particle_ptr1->set_velocity(u_new);
			new_particle_ptr1->set_weight(weight_temp);
			new_particle_ptr1->set_h(h_temp);
			particles_ptr_list_.push_back(new_particle_ptr1);
	
			getline(SPH_particles_file, text_string);
		}
	}
	
	SPH_particles_file.close();
}


void SPH_Driver::generate_mesh(const myreal size_x, const myreal size_y, 
	const myreal size_z, const myreal dx, const myreal dy, const myreal dz) {
	// This function generates the mesh given size of the box and spacing in 
	// each direction
	SPH_Mesh temp_mesh(size_x, size_y, size_z, dx, dy, dz);
	mesh_ = temp_mesh;
}


void SPH_Driver::update_neighboring_particles_ref() {
	// This function updates the neighbor_list for all particles in 
	// particles_ptr_list. 
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		particle_i_ptr->clear_neighbor_list(); // clear neighbor_list
		fourvec x_i = particle_i_ptr->get_position(); // get position

		for (const auto & particle_j_ptr : particles_ptr_list_) {
			// check distance form particle_j to particle_i
			if (particle_j_ptr->check_distance(x_i)) {
				// if within support of W, add to neighbor list
				particle_i_ptr->add_to_neighbor_list(particle_j_ptr);
			}
		}
	}
}


void SPH_Driver::update_neighboring_particles() {
	// This function updates the neighbor_list for all particles in 
	// particles_ptr_list
	// get the number of cells of the mesh in each direction
	int Nx = mesh_.get_Nx();
	int Ny = mesh_.get_Ny();
	int Nz = mesh_.get_Nz();

	// update the linked_list for each cell in the mesh_
	mesh_.update_linked_list(particles_ptr_list_);

	for (int idx_x = 0; idx_x < Nx; idx_x++) {
		std::vector<int> neighbor_x_idx(1, 0);
		if (idx_x != 0) {
			neighbor_x_idx.push_back(-1);
		}
		if (idx_x != Nx - 1) {
			neighbor_x_idx.push_back(1);
		} 

	for (int idx_y = 0; idx_y < Ny; idx_y++) {
		std::vector<int> neighbor_y_idx(1, 0);
		if (idx_y != 0) {
			neighbor_y_idx.push_back(-1);
		}
		if (idx_y != Ny - 1) {
			neighbor_y_idx.push_back(1);
		} 

	for (int idx_z = 0; idx_z < Nz; idx_z++) {
		SPH_Cell temp_cell = mesh_.get_cell(idx_x, idx_y, idx_z);
		std::vector<int> neighbor_z_idx(1, 0);
		if (idx_z != 0) {
			neighbor_z_idx.push_back(-1);
		}
		if (idx_z != Nz - 1) {
			neighbor_z_idx.push_back(1);
		}

		for (const auto & particle_i_ptr : temp_cell.get_linked_list()) {
		particle_i_ptr.lock()->clear_neighbor_list(); // clear neighbor_list
		fourvec x_i = particle_i_ptr.lock()->get_position(); // get position

		for (const auto dx_idx : neighbor_x_idx) {
		for (const auto dy_idx : neighbor_y_idx) {
		for (const auto dz_idx : neighbor_z_idx) {
			SPH_Cell temp_cell2 = mesh_.get_cell(idx_x + dx_idx, idx_y + dy_idx,
				idx_z + dz_idx);
			for (const auto & particle_j_ptr : temp_cell2.get_linked_list()) {
				// check distance form particle_j to particle_i
				if (particle_j_ptr.lock()->check_distance(x_i)) {
					// if within support of W, add to neighbor list
					particle_i_ptr.lock()->add_to_neighbor_list(
						particle_j_ptr.lock());
				}
			}

		}
		}
		}
		}
	}
	}
	}
}


void SPH_Driver::update_position(const myreal delta_t) {
	// This function updates the position for all particles.
	for (auto & particle_i_ptr : particles_ptr_list_) {
		fourvec current_x = particle_i_ptr->get_position(); // get position
		fourvec current_u = particle_i_ptr->get_velocity(); // get four velocity
		// divide by u_0 to get the actual velocities v_x, v_y, v_z
		fourvec current_v = current_u / current_u[0];
		fourvec new_x;
		
		new_x = current_x + current_v * delta_t; // update position

		// introduce periodic boundary conditions
		myreal boxwidth = 6.;
		for (int i = 1; i < 4; i++) {
			if(new_x[i]>boxwidth/2.) {
				new_x.set_a_i(i, new_x[i]-boxwidth);
			}
			if(new_x[i]< -boxwidth/2.) {
				new_x.set_a_i(i, new_x[i]+boxwidth);
			}
		}

		particle_i_ptr->set_position(new_x); // set position to new_x
	}
}


void SPH_Driver::update_velocity() {
	// This function updates the velocity for all particles.
	for (auto & particle_i_ptr : particles_ptr_list_) {
		fourvec current_x = particle_i_ptr->get_position(); // get position
		fourvec new_u; 

		// get new velocity (constant or at rest)
		new_u.set_a_i(0, 1.);
		new_u.set_a_i(1, 0.); // at rest
		// new_u.set_a_i(1, std::sqrt(3.)/2.);  // constant x velocity
		new_u.set_a_i(2, 0.);
		new_u.set_a_i(3, 0.);

		// get new velocity (radial)
		// myreal a = 0.5; // coefficient for eta
		// myreal eta = a * current_x.three_norm(); // eta increases radially
		// myreal norm_v = tanh(eta); // use tanh to keep |v| between -1 and 1
		// // v = |v| * r/|r|
		// new_u = current_x * (norm_v/(current_x.three_norm()+small_eps)); 
		// new_u.set_a_i(0, 1.); // reset u_0 to be correct

		new_u = new_u / new_u.norm(); // normalize velocity

		particle_i_ptr->set_velocity(new_u); // set velocity to new_u
	}
}


myreal SPH_Driver::compute_rho(const fourvec &x) {
	// This function computes rho at an arbitrary position x. 
	myreal rho = 0.;
	for (const auto & particle_j_ptr : particles_ptr_list_) {
		// check the distance of particle_j to x
		if (particle_j_ptr->check_distance(x)) {
			// if within support of W, add contribution towards rho
			myreal mass_j = particle_j_ptr->get_mass();
			myreal W_ij = particle_j_ptr->W(x);

			rho += mass_j * W_ij;
		}
	}

	return(rho);
}


void SPH_Driver::update_rho() {
	// This function updates rho for all particles.
	for (auto & particle_i_ptr : particles_ptr_list_) {
		myreal new_rho_i = 0.;
		fourvec x_i = particle_i_ptr->get_position(); // get particle_i position

		// loop over particles in neighbor_list of particle_i and add to rho
		for (auto & particle_j_ptr : particle_i_ptr->get_neighbor_list()) {
			myreal mass_j = particle_j_ptr.lock()->get_mass();
			myreal W_ij = particle_j_ptr.lock()->W(x_i);

			new_rho_i += mass_j * W_ij;
		}

		particle_i_ptr->set_rho(new_rho_i);
	}
}


void SPH_Driver::evolve_system(const myreal delta_t) {
	// This function evolves the system for time delta_t
	update_position(delta_t);
	update_velocity();
	update_neighboring_particles();
	update_rho();
}


fourvec SPH_Driver::get_u_background(const fourvec &x) {
	// This function returns the background four velocity at a position x
	fourvec u(1., 0., 0., 0.); // background is at rest
	return(u);
}


myreal SPH_Driver::get_theta(const fourvec &x) {
	// This function returns theta, the expansion rate, at a position x
	return(0.); // return 0 for now
} 


myreal SPH_Driver::get_shear_viscosity(const fourvec &x) {
	// This fucntion returns the shear viscosity at a position x
	return(0.); // return 0 for now (no viscosity)
}


myreal SPH_Driver::get_c_s_sq(const fourvec &x) {
	// This function returns the speed of sound squared, c_s^2, of the fluid at 
	// a position x
	return(1./3.); // return constant 1./3.
}


myreal SPH_Driver::get_c_s(const fourvec &x) {
	// This function returns the speed of sound, c_s, of the fluid at a position
	// x
	myreal c_s_sq = get_c_s_sq(x);
	return(sqrt(c_s_sq)); // return constant sqrt(1./3.)
}


std::vector<myreal> SPH_Driver::compute_weight(const fourvec &x) {
	// This function computes the weight vector at an arbitrary x
	std::vector<myreal> weight; // initialize empty vector
	int weight_size = -1;
	for (const auto & particle_j_ptr : particles_ptr_list_) {
		// for first particle j, get the size of weight vector
		if (weight_size == -1) {
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			weight_size = weight_j.size();
			// set return weight vector to have length weight_size
			weight = std::vector<myreal> (weight_size, 0.);
		}
		// check the distance of particle_j to x
		if (particle_j_ptr->check_distance(x)) {
			// if within support of W, add contribution to weight
			myreal mass_j = particle_j_ptr->get_mass();
			myreal W_ij = particle_j_ptr->W(x);
			myreal rho_j = particle_j_ptr->get_rho();
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			// loop over weight vector
			for (int i = 0; i < weight_size; i++) {
				weight[i] += (mass_j / rho_j) * weight_j[i] * W_ij; 
			}
		}
	}

	return(weight);
}


std::vector<myreal> SPH_Driver::compute_dx_weight(const fourvec &x) {
	// This function computes the x derivative of weight vector at any x
	std::vector<myreal> result; // initialize empty vector
	int weight_size = -1;
	for (const auto & particle_j_ptr : particles_ptr_list_) {
		// for first particle j, get the size of weight vector
		if (weight_size == -1) {
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			weight_size = weight_j.size();
			// set return weight vector to have length weight_size
			result = std::vector<myreal> (weight_size, 0.);
		}
		// check the distance of particle_j to x
		if (particle_j_ptr->check_distance(x)) {
			// if within support of W, add contribution to derivative of weight
			myreal mass_j = particle_j_ptr->get_mass();
			myreal rho_j = particle_j_ptr->get_rho();
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			myreal dx_W_ij = particle_j_ptr->dx_W(x);

			// loop over weight vector
			for (int i = 0; i < weight_size; i++) {
				result[i] += -(mass_j/ rho_j) * weight_j[i] * dx_W_ij;
			}
		}
	}

	return(result);
}


std::vector<myreal> SPH_Driver::compute_eq_weight(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the equilibrium weight at an arbitrary particle
	std::vector<myreal> result (particle_ptr->get_weight().size(), 0.);

	return(result);  // do nothing at the moment
}


std::vector<myreal> SPH_Driver::compute_relax_source(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the source term at an arbitrary particle
	std::vector<myreal> weight = particle_ptr->get_weight();
	std::vector<myreal> eq_weight = compute_eq_weight(particle_ptr);
	fourvec u = particle_ptr->get_velocity(); // get particle_ptr velocity
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	myreal Gamma = 0.; // coefficient in source term

	// loop over weight vector and calculate source term
	for (unsigned int i = 0; i < weight.size(); i++) {
		result[i] = -Gamma/u[0]*(weight[i]-eq_weight[i]);
	}

	return(result);
}


std::vector<myreal> SPH_Driver::compute_matrix_source(
            const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the matrix source term at any particle.
	// This function assumes that weight is a 5 dimensional vector, as follows:
	// weight = (delta e, delta g^t, delta g^x, delta g^y, delta g^z).
	// This function also currently assumes that u^mu = (1,0,0,0) everywhere.
	std::vector<myreal> weight = particle_ptr->get_weight();
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	fourvec x = particle_ptr->get_position();

	myreal theta = get_theta(x);
	myreal gamma_eta = get_shear_viscosity(x);
	myreal c_s_sq = get_c_s_sq(x);
	myreal c_s = get_c_s(x);

	for (auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		myreal rho_j = particle_j_ptr.lock()->get_rho();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx_W_ij = particle_j_ptr.lock()->dx_W(x);
		myreal dy_W_ij = particle_j_ptr.lock()->dy_W(x);
		myreal dz_W_ij = particle_j_ptr.lock()->dz_W(x);

		//! contribution from L matrix
		// compute divergence of (delta g^x, delta g^y, delta g^z)
		myreal div = weight_j[2]*dx_W_ij + weight_j[3] * dy_W_ij 
			+ weight_j[4] * dz_W_ij;
		// ccompute contribution to source term
		result[0] += -c_s * mass_j / rho_j * div;
		result[2] += -c_s * mass_j / rho_j * weight_j[0] * dx_W_ij;
		result[3] += -c_s * mass_j / rho_j * weight_j[0] * dy_W_ij;
		result[4] += -c_s * mass_j / rho_j * weight_j[0] * dz_W_ij;
	}

	//! contribution from K matrix
	result[0] += -(1. + c_s_sq) * theta * weight[0];
	result[2] += -theta * weight[2];
	result[3] += -theta * weight[3];
	result[4] += -theta * weight[4];

	//! make sure that result[1] = 0, as it should right now be
	result[1] = 0;

	return(result);
}


std::vector<myreal> SPH_Driver::compute_diffusion1(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the source term from diffusion at an arbitrary 
	// particle using the basic SPH scheme
	std::vector<myreal> weight = particle_ptr->get_weight();
	fourvec x = particle_ptr->get_position(); // get particle_ptr position
	fourvec u = particle_ptr->get_velocity(); // get particle_ptr velocity
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	myreal D = 1.; // coefficient in diffusion term
	int dim = particle_ptr->get_dim(); // dimension of particle for laplacian

	// loop over particles in neighbor_list of particle_ptr and add source term
	for (auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		myreal rho_j = particle_j_ptr.lock()->get_rho();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx2_W_ij = particle_j_ptr.lock()->dx2_W(x);
		myreal dy2_W_ij = particle_j_ptr.lock()->dy2_W(x);
		myreal dz2_W_ij = particle_j_ptr.lock()->dz2_W(x);
		myreal lap_W_ij = 0.;

		// calculate laplacian based on dimension
		if (dim == 1) {
			lap_W_ij = dx2_W_ij;
		}
		else if (dim == 2) {
			lap_W_ij = dx2_W_ij + dy2_W_ij;
		}
		else if (dim == 3) {
			lap_W_ij = dx2_W_ij + dy2_W_ij + dz2_W_ij;
		}

		myreal temp = mass_j/rho_j * lap_W_ij; // temporary value

		// loop over weight vector and calculate diffusion source term
		for (unsigned int i = 0; i < weight.size(); i++) {
			result[i] += D/u[0] * temp * (weight_j[i]);
		}
	}

	return(result);
}


std::vector<myreal> SPH_Driver::compute_diffusion2(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the source term from diffusion at an arbitrary 
	// particle using an antisymmetrized SPH scheme
	std::vector<myreal> weight = particle_ptr->get_weight();
	fourvec x = particle_ptr->get_position(); // get particle_ptr position
	fourvec u = particle_ptr->get_velocity(); // get particle_ptr velocity
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	myreal D = 1.; // coefficient in diffusion term
	int dim = particle_ptr->get_dim(); // dimension of particle for laplacian

	// loop over particles in neighbor_list of particle_ptr and add source term
	for (auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		myreal rho_j = particle_j_ptr.lock()->get_rho();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx2_W_ij = particle_j_ptr.lock()->dx2_W(x);
		myreal dy2_W_ij = particle_j_ptr.lock()->dy2_W(x);
		myreal dz2_W_ij = particle_j_ptr.lock()->dz2_W(x);
		myreal lap_W_ij = 0.;

		// calculate laplacian based on dimension
		if (dim == 1) {
			lap_W_ij = dx2_W_ij;
		}
		else if (dim == 2) {
			lap_W_ij = dx2_W_ij + dy2_W_ij;
		}
		else if (dim == 3) {
			lap_W_ij = dx2_W_ij + dy2_W_ij + dz2_W_ij;
		}

		myreal temp = mass_j/rho_j * lap_W_ij; // temporary value

		// loop over weight vector and calculate diffusion source term
		for (unsigned int i = 0; i < weight.size(); i++) {
			result[i] += D/u[0] * temp * (weight_j[i]-weight[i]);
		}
	}

	return(result);
}


std::vector<myreal> SPH_Driver::compute_diffusion3(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the source term from diffusion at an arbitrary 
	// particle using a symmetrized SPH scheme
	std::vector<myreal> weight = particle_ptr->get_weight();
	fourvec x = particle_ptr->get_position(); // get particle_ptr position
	fourvec u = particle_ptr->get_velocity(); // get particle_ptr velocity
	myreal rho = particle_ptr->get_rho(); // get particle_ptr density
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	myreal D = 1.; // coefficient in diffusion term
	int dim = particle_ptr->get_dim(); // dimension of particle for laplacian

	// loop over particles in neighbor_list of particle_ptr and add source term
	for (auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		myreal rho_j = particle_j_ptr.lock()->get_rho();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx2_W_ij = particle_j_ptr.lock()->dx2_W(x);
		myreal dy2_W_ij = particle_j_ptr.lock()->dy2_W(x);
		myreal dz2_W_ij = particle_j_ptr.lock()->dz2_W(x);
		myreal lap_W_ij = 0.;

		// calculate laplacian based on dimension
		if (dim == 1) {
			lap_W_ij = dx2_W_ij;
		}
		else if (dim == 2) {
			lap_W_ij = dx2_W_ij + dy2_W_ij;
		}
		else if (dim == 3) {
			lap_W_ij = dx2_W_ij + dy2_W_ij + dz2_W_ij;
		}

		myreal temp = rho * mass_j * lap_W_ij; // temporary value

		// loop over weight vector and calculate diffusion source term
		for (unsigned int i = 0; i < weight.size(); i++) {
			result[i] += D/u[0] * temp * (weight_j[i]/(rho_j*rho_j) 
				+ weight[i]/(rho * rho));
		}
	}

	return(result);
}


std::vector<myreal> SPH_Driver::compute_causal_diff(
            const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the causal diffusion source term at any particle. 
	// This function assumes that weight is a 5 dimensional vector, as follows: 
	// weight = (n, q^t, q^x, q^y, q^z)
	std::vector<myreal> weight = particle_ptr->get_weight();
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	fourvec x = particle_ptr->get_position();
	fourvec u = particle_ptr->get_velocity();

	myreal gamma = u[0];
	myreal gam_sq = gamma * gamma;
	myreal vx = u[1]/gamma;
	myreal vy = u[2]/gamma;
	myreal vz = u[3]/gamma;

	myreal theta = get_theta(x);
	myreal tau = 1.;
	myreal D = 1.;

	// constant factor factored out of matrix
	myreal factor = (gam_sq * tau - D * (gam_sq - 1.));

	// gradient and divergence terms
	for (auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		myreal rho_j = particle_j_ptr.lock()->get_rho();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx_W_ij = particle_j_ptr.lock()->dx_W(x);
		myreal dy_W_ij = particle_j_ptr.lock()->dy_W(x);
		myreal dz_W_ij = particle_j_ptr.lock()->dz_W(x);

		// compute divergence of (q^x, q^y, q^z)
		myreal div_q = weight_j[2] * dx_W_ij + weight_j[3] * dy_W_ij 
			+ weight_j[4] * dz_W_ij;
		// compute contribution of v^k d_k n and v^k d_k q^t
		myreal vkdk = vx * dx_W_ij + vy * dy_W_ij + vz * dz_W_ij;
		myreal vkdk_n = vkdk * weight_j[0];
		myreal vkdk_qt = vkdk * weight_j[1];

		// compute contribution of differential terms
		result[0] += mass_j/rho_j * (D * vkdk_n + gamma * tau * vkdk_qt 
			- gamma * tau * div_q);
		result[1] += mass_j/rho_j * (-D * gamma * vkdk_n 
			- D * (gam_sq - 1.) * vkdk_qt + D * (gam_sq - 1.) * div_q);
		result[2] += mass_j/rho_j * ((-D * factor * weight_j[0] * dx_W_ij 
			- D * D * gamma * u[1] * vkdk_n)/(gamma * tau) 
			- D * gamma * u[1] * vkdk_qt + D * gamma * u[1] * div_q);
		result[3] += mass_j/rho_j * ((-D * factor * weight_j[0] * dy_W_ij 
			- D * D * gamma * u[2] * vkdk_n)/(gamma * tau) 
			- D * gamma * u[2] * vkdk_qt + D * gamma * u[2] * div_q);
		result[4] += mass_j/rho_j * ((-D * factor * weight_j[0] * dz_W_ij 
			- D * D * gamma * u[3] * vkdk_n)/(gamma * tau) 
			- D * gamma * u[3] * vkdk_qt + D * gamma * u[3] * div_q);
	}

	// non differential terms
	result[0] += -gamma * theta * tau * weight[0] + weight[1];
	result[1] += D * (gam_sq - 1.) * theta * weight[0] - gamma * weight[1];
	result[2] += D * gamma * u[1] * theta * weight[0] 
		- D * u[1] * weight[1]/tau - factor/(gamma * tau) * weight[2];
	result[3] += D * gamma * u[2] * theta * weight[0] 
		- D * u[2] * weight[1]/tau - factor/(gamma * tau) * weight[3];
	result[4] += D * gamma * u[3] * theta * weight[0] 
		- D * u[3] * weight[1]/tau - factor/(gamma * tau) * weight[4];

	// divide by overall constant factor (gamma^2 * tau - D * (gamma^2 - 1))
	for (int i = 0; i < 5; i++) {
		result[i] = result[i] / factor;
	}

	return(result);
}

void SPH_Driver::euler_step(const myreal delta_t) {
	// This function computes one euler time step in the differential equation
	std::vector<std::vector<myreal>> next_weights; // vector of next weights
	int i = 0;
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		// get weight and source from particle_i
		std::vector<myreal> source = compute_relax_source(particle_i_ptr);
		std::vector<myreal> weight = particle_i_ptr->get_weight();
		std::vector<myreal> new_weight (weight.size(), 0.);

		// compute new_weight
		for (unsigned int j = 0; j < weight.size(); j++) {
			new_weight[j] = weight[j] + delta_t*source[j];
		}

		// add new_weight to next_weights vector
		next_weights.push_back(new_weight);
		i++;
	}
	// set next_weights
	for (unsigned int k = 0; k < particles_ptr_list_.size(); k++) {
		particles_ptr_list_[k]->set_weight(next_weights[k]);
	}

	// evolve system for delta_t time
	evolve_system(delta_t);
}


void SPH_Driver::generalized_RK_2(const myreal delta_t, const myreal alpha) {
	// This function computes one generalized RK_2 time step in the differential 
	// equation with parameter alpha, where 0 <= alpha <= 1.
	// vector runge kutta steps for each particle
	std::vector<std::vector<myreal>> k1;
	std::vector<std::vector<myreal>> k2;

	// vectors of intermediate weights for each particle
	std::vector<std::vector<myreal>> weights_0; // current weights
	std::vector<std::vector<myreal>> weights_temp; // y_n + h * alpha * k1

	int i = 0; // integere to count particles

	// first RK step
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		// get current weight from particle_i and push_back onto weights_0
		std::vector<myreal> weight = particle_i_ptr->get_weight();
		weights_0.push_back(weight);

		// get source from particle_i
		std::vector<myreal> source = compute_causal_diff(particle_i_ptr);

		// weight after 1st RK step
		std::vector<myreal> new_weight (weight.size(), 0.);
		// compute rhs and push_back onto k1. Also, compute new_weight and 
		// push_back onto weights_1
		for (unsigned int j = 0; j < source.size(); j++) {
			new_weight[j] = weights_0[i][j] + delta_t * alpha * source[j];
		}
		k1.push_back(source);
		weights_temp.push_back(new_weight);

		i++;
	}

	// evolve system for alpha * delta_t time to compute 2nd RK step
	evolve_system(alpha * delta_t);

	// set weights to weights_1 for the 2nd RK step
	for (unsigned int k = 0; k < particles_ptr_list_.size(); k++) {
		particles_ptr_list_[k]->set_weight(weights_temp[k]);
	}

	i = 0; // reset particle counter

	// 2nd RK step
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		// get source from particle_i and push_back onto k2
		std::vector<myreal> source = compute_causal_diff(particle_i_ptr);
		k2.push_back(source);

		i++;
	}

	// evolve system for remaining (1. - alpha) * delta_t time
	evolve_system((1.-alpha) * delta_t);

	// set weights at t + delta_t
	for (unsigned int k = 0; k < particles_ptr_list_.size(); k++) {
		std::vector<myreal> new_weight (weights_0[k].size(), 0.);
		// new_weight = weight_0 + delta_t*[(1-1/(2alpha))*k1 + 1/(2alpha)*k2]
		for (unsigned int j = 0; j < weights_0[k].size(); j++) {
			new_weight[j] = weights_0[k][j] + delta_t * 
				((1. - 1./(2.*alpha)) * k1[k][j] + 1./(2.*alpha) * k2[k][j]);
		}
		// set new_weight
		particles_ptr_list_[k]->set_weight(new_weight);
	}
}


void SPH_Driver::RK_4(const myreal delta_t) {
	// This function computes one RK_4 time step in the differential equation

	// vector runge kutta steps for each particle
	std::vector<std::vector<myreal>> k1;
	std::vector<std::vector<myreal>> k2;
	std::vector<std::vector<myreal>> k3;
	std::vector<std::vector<myreal>> k4;

	// vectors of intermediate weights for each particle
	std::vector<std::vector<myreal>> weights_0; // current weights
	std::vector<std::vector<myreal>> weights_temp; // temp weights at each step

	int i = 0; // integer to count particles

	// first RK step
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		// get current weight from particle_i and push_back onto weights_0
		std::vector<myreal> weight = particle_i_ptr->get_weight();
		weights_0.push_back(weight);

		// get source from particle_i
		std::vector<myreal> source = compute_causal_diff(particle_i_ptr);

		// weight after 1st RK step
		std::vector<myreal> new_weight (weight.size(), 0.);
		// compute rhs and push_back onto k1. Also, compute new_weight and 
		// push_back onto weights_1
		for (unsigned int j = 0; j < source.size(); j++) {
			new_weight[j] = weights_0[i][j] + delta_t/2. * source[j];
		}
		k1.push_back(source);
		weights_temp.push_back(new_weight);

		i++;
	}

	// evolve system for delta_t/2. time to compute 2nd and 3rd RK steps
	evolve_system(delta_t/2.);

	// set weights to weights_1 for the 2nd RK step
	for (unsigned int k = 0; k < particles_ptr_list_.size(); k++) {
		particles_ptr_list_[k]->set_weight(weights_temp[k]);
	}
	weights_temp.clear(); // clear temporary weights

	i = 0; // reset particle counter

	// 2nd RK step
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		// get source from particle_i
		std::vector<myreal> source = compute_causal_diff(particle_i_ptr);

		// weight after 2nd RK step
		std::vector<myreal> new_weight (source.size(), 0.);
		// compute rhs and push_back onto k2. Also, compute new_weight and 
		// push_back onto weights_2
		for (unsigned int j = 0; j < source.size(); j++) {
			new_weight[j] = weights_0[i][j] + delta_t/2. * source[j];
		}
		k2.push_back(source);
		weights_temp.push_back(new_weight);

		i++;
	}

	// set weights to weights_2 for the 3rd RK step
	for (unsigned int k = 0; k < particles_ptr_list_.size(); k++) {
		particles_ptr_list_[k]->set_weight(weights_temp[k]);
	}
	weights_temp.clear(); // clear temporary weights

	i = 0; // reset particle counter

	// 3rd RK step
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		// get source from particle_i
		std::vector<myreal> source = compute_causal_diff(particle_i_ptr);

		// weight after 3rd RK step
		std::vector<myreal> new_weight (source.size(), 0.);
		// compute rhs and push_back onto k3. Also, compute new_weight and 
		// push_back onto weights_3
		for (unsigned int j = 0; j < source.size(); j++) {
			new_weight[j] = weights_0[i][j] + delta_t * source[j];
		}
		k3.push_back(source);
		weights_temp.push_back(new_weight);

		i++;
	}

	// evolve system for delta_t/2. time to compute 4th RK step
	evolve_system(delta_t/2.);

	// set weights to weights_3 for the 4th RK step
	for (unsigned int k = 0; k < particles_ptr_list_.size(); k++) {
		particles_ptr_list_[k]->set_weight(weights_temp[k]);
	}
	weights_temp.clear();

	i = 0; // reset particle counter

	// 4th RK step
	for (const auto & particle_i_ptr : particles_ptr_list_) {
		// get source from particle_i and push_back onto k4
		std::vector<myreal> source = compute_causal_diff(particle_i_ptr);
		k4.push_back(source);

		i++;
	}

	// set weights at t + delta_t
	for (unsigned int k = 0; k < particles_ptr_list_.size(); k++) {
		std::vector<myreal> new_weight (weights_0[k].size(), 0.);
		// new_weight = weight_0 + (delta_t/6.)*(k1 + 2k2 + 2k3 + k4)
		for (unsigned int j = 0; j < weights_0[k].size(); j++) {
			new_weight[j] = weights_0[k][j] + delta_t/6. * (k1[k][j] 
				+ 2.*k2[k][j] + 2.*k3[k][j] + k4[k][j]);
		}
		// set new_weight
		particles_ptr_list_[k]->set_weight(new_weight);
	}
}


void SPH_Driver::ODE_solver(const myreal t_init, const myreal t_final,
                        	const myreal delta_t) {
	// This function solves the ODE from t_init to t_final with steps delta_t

	// generate the mesh given the width from a particle
	myreal temp_h = particles_ptr_list_[0]->get_h();
	generate_mesh(6., 0., 0., 5.*temp_h, 5.*temp_h, 5.*temp_h);

	// std::ofstream out_relax("relaxation_test/weight_2.dat");
	// out_relax << "#  t   phi_0(x) for x from -3 to 3 at steps dx = 0.01, "
	// 	<< "121 particles in 6 width box, h = 0.1, Gamma = 1., vx = sqrt(3)/2"
	// 	<< std::endl;

	// std::ofstream out_h("h_test/h_test_9.dat");
	// out_h << "#  t   phi_0(x) for x from -3 to 3 at steps dx = 0.01, "
	// 	<< "61 particles in 6 width box, h = 0.085"
	// 	<< std::endl;

	// initialize output streams to files for data
	// std::ofstream out_weight1("shockwave_init_weight_gaussian.dat");
	// out_weight1 << "#   x   phi_0(x) at t=0. for x from -5 to 5 "
	// 	<< "at steps delta_x = 0.01" 
	// 	<< std::endl;
// 
	// std::ofstream out_weight2("shockwave_weight_0.2_gaussian.dat");
	// out_weight2 << "#   x   phi_0(x) at t=2. for x from -5 to 5 "
	// 	<< "at steps delta_x = 0.01" 
	// 	<< std::endl;
// 
// 
	// std::ofstream out_weight3("shockwave_weight_0.4_gaussian.dat");
	// out_weight3 << "#   x   phi_0(x) at t=4. for x from -5 to 5 "
	// 	<< "at steps delta_x = 0.01" 
	// 	<< std::endl;
// 
	// std::ofstream out_rho1("shockwave_init_rho_gaussian.dat");
	// out_rho1 << "#   x   rho(x) at t=0. for x from -5 to 5 "
	// 	<< "at steps delta_x = 0.01" 
	// 	<< std::endl;
// 
	// std::ofstream out_rho2("shockwave_rho_0.2_gaussian.dat");
	// out_rho2 << "#   x   rho(x) at t=2. for x from -5 to 5 "
	// 	<< "at steps delta_x = 0.01" 
	// 	<< std::endl;
// 
// 
	// std::ofstream out_rho3("shockwave_rho_0.4_gaussian.dat");
	// out_rho3 << "#   x   rho(x) at t=4. for x from -5 to 5 "
	// 	<< "at steps delta_x = 0.01" 
	// 	<< std::endl;

	// std::ofstream out_delta("delta_function_test/delta_func_1d_18.dat");
	// out_delta << "#   t   phi_0(x) for x from -2 to 2 at steps delta = 0.05, "
	// 	<< "h = 0.1, 3721 particles in 6x6, D = 1., dt = 0.02, scheme 2"
	// 	<< std::endl;
// 
	// std::ofstream out_delta2("delta_function_test/delta_func_2d_18.dat");
	// out_delta2 << "#   t   phi_0(x) for x,y from -2 to 2 at steps delta = 0.05, "
	// 	<< "h = 0.1, 3721 particles in 6x6, D = 1., dt = 0.02, scheme 2"
	// 	<< std::endl;

	// std::ofstream out_diff("diffusion_test/weight_1_12.dat");
	// out_diff << "#   t   phi_0(x) for x from -2 to 2 at steps dx = 0.05, "
	// 	<< "61 particles in 6 width box, h = 0.1, D = 1, dt = 0.05, scheme 1, " 
	// 	<< "euler" << std::endl;
// 
	// std::ofstream out_diff2("diffusion_test/lap_1_12.dat");
	// out_diff2 << "#   t   lap(phi_0(x)) for x from -2 to 2 at steps dx = 0.05, "
	// 	<< "61 particles in 6 width box, h = 0.1, D = 1, dt = 0.05, scheme 1, " 
	// 	<< "euler" << std::endl;
// 
	// std::ofstream out_diff3("diffusion_test/diffusion_1_12.dat");
	// out_diff3 << "#   t   lap(phi_0(x)) for x in particles_ptr_list_, "
	// 	<< "61 particles in 6 width box, h = 0.1, D = 1, dt = 0.05, scheme 1, " 
	// 	<< "euler" << std::endl;
// 
	// std::ofstream out_diff4("diffusion_test/x_pts_1_12.dat");
	// out_diff4 << "#   t   x for particles in particles_ptr_list_, "
	// 	<< "61 particles in 6 width box, h = 0.1, D = 1, dt = 0.05, scheme 1, " 
	// 	<< "euler" << std::endl;

	// std::ofstream out_0("causal_diff_test/weight[0]_const_v_1.dat");
	// out_0 << "#  t   phi_0(x) for x from -2 to 2 at steps dx = 0.05, "
	// 	<< "121 particles in 6 width box, h = 0.1, dt = 0.01, D = 1, tau = 1, "
	// 	<< "vx = 0." << std::endl;
// 
	// std::ofstream out_1("causal_diff_test/weight[1]_const_v_1.dat");
	// out_1 << "#  t   phi_1(x) for x from -2 to 2 at steps dx = 0.05, "
	// 	<< "121 particles in 6 width box, h = 0.1, dt = 0.01, D = 1, tau = 1, "
	// 	<< "vx = 0." << std::endl;
// 
	// std::ofstream out_2("causal_diff_test/weight[2]_const_v_1.dat");
	// out_2 << "#  t   phi_2(x) for x from -2 to 2 at steps dx = 0.05, "
	// 	<< "121 particles in 6 width box, h = 0.1, dt = 0.01, D = 1, tau = 1, "
	// 	<< "vx = 0." << std::endl;
// 
	// std::ofstream out_3("causal_diff_test/weight[3]_const_v_1.dat");
	// out_3 << "#  t   phi_3(x) for x from -2 to 2 at steps dx = 0.05, "
	// 	<< "121 particles in 6 width box, h = 0.1, dt = 0.01, D = 1, tau = 1, "
	// 	<< "vx = 0." << std::endl;
// 
	// std::ofstream out_4("causal_diff_test/weight[4]_const_v_1.dat");
	// out_4 << "#  t   phi_4(x) for x from -2 to 2 at steps dx = 0.05, "
	// 	<< "121 particles in 6 width box, h = 0.1, dt = 0.01, D = 1, tau = 1, "
	// 	<< "vx = 0." << std::endl;

	update_neighboring_particles();
	update_rho();

	int j = 0; // integer that keeps track of time step
	myreal vx = sqrt(3.)/2.;

	// loop over time interval
	for(double t = t_init; t <= t_final + delta_t/2.; t += delta_t) {
		// compute weights at each time step for relaxation test
		// for (double x = -3.; x<= 3.005; x+=0.01) {
		// 	out_relax << std::setw(5) << t << " ";
		// 	fourvec x_temp(t, x, 0., 0.);
		// 	auto weight_temp = compute_weight(x_temp);
// 
		// 	out_relax << std::setw(16) << std::setprecision(6) 
		// 		<< weight_temp[0] << std::endl;
		// }

		// compute weights at each time step for relaxation test
		// for (double x = -3.; x<= 3.005; x+=0.01) {
		// 	out_h << std::setw(5) << t << " ";
		// 	fourvec x_temp(t, x, 0., 0.);
		// 	auto weight_temp = compute_weight(x_temp);
// 
		// 	out_h << std::setw(16) << std::setprecision(6) 
		// 		<< weight_temp[0] << std::endl;
		// }

		// compute weights at each time step at specific values of x
		// for (double x = -2.; x <= 2.005; x+=0.01) {
		// 	out_0 << std::setw(5) << t << " ";
		// 	out_1 << std::setw(5) << t << " ";
		// 	out_2 << std::setw(5) << t << " ";
		// 	out_3 << std::setw(5) << t << " ";
		// 	out_4 << std::setw(5) << t << " ";
// 
		// 	fourvec x_temp(t, (x+vx*t), 0., 0.);
		// 	std::vector<myreal> weight_temp = compute_weight(x_temp);
// 
		// 	out_0 << std::setw(16) << std::setprecision(6) << weight_temp[0] 
		// 	    << std::endl;
		// 	out_1 << std::setw(16) << std::setprecision(6) << weight_temp[1] 
		// 	    << std::endl;
		// 	out_2 << std::setw(16) << std::setprecision(6) << weight_temp[2] 
		// 	    << std::endl;
		// 	out_3 << std::setw(16) << std::setprecision(6) << weight_temp[3] 
		// 	    << std::endl;
		// 	out_4 << std::setw(16) << std::setprecision(6) << weight_temp[4] 
		// 	    << std::endl;
		// }

		// compute weights at each time step at specific values of x,y
		// for (double x = -2.; x<= 2.005; x+= 0.025) {
		// 	out_delta << std::setw(5) << t << " ";
		// 	fourvec x_temp(t, x, 0., 0.);
		// 	std::vector<myreal> weight_temp = compute_weight(x_temp);
		// 	out_delta << std::setw(16) << std::setprecision(6) << weight_temp[0] 
		//  	    << std::endl;
// 
		// 	for (double y = -2.; y<= 2.005; y+= 0.025) {
		// 		out_delta2 << std::setw(5) << t << " ";
		// 		fourvec x_temp2(t, x, y, 0.);
		// 		std::vector<myreal> weight_temp2 = compute_weight(x_temp2);
		// 		out_delta2 << std::setw(16) << std::setprecision(6) 
		// 			<< weight_temp2[0] << std::endl;
		// 	}
		// }

		// // compute weights at each time step at specific values of x
		// for (double x = -2.; x <= 2.005; x+=0.01) {
		// 	out_diff << std::setw(5) << t << " ";
		// 	fourvec x_temp(t, x, 0., 0.);
		// 	std::vector<myreal> weight_temp = compute_weight(x_temp);
		// 	out_diff << std::setw(16) << std::setprecision(6) << weight_temp[0] 
		// 	    << std::endl;
		// }
// 
		// // compute laplacian at each time step at specific values of x
		// for (double x = -2.; x <= 2.005; x+=0.01) {
		// 	out_diff2 << std::setw(5) << t << " ";
		// 	fourvec x_temp(t, x, 0., 0.);
		// 	std::vector<myreal> laplacian_temp = compute_laplacian1(x_temp);
		// 	out_diff2 << std::setw(16) << std::setprecision(6) 
		// 		<< laplacian_temp[0] << std::endl;
		// }
// 
		// // compute diffusion at each time step at every particle
		// for (auto const particle_i_ptr : particles_ptr_list_) {
		// 	out_diff3 << std::setw(5) << t << " ";
		// 	auto diffusion_temp = compute_diffusion1(particle_i_ptr);
		// 	out_diff3 << std::setw(16) << std::setprecision(6) 
		// 		<< diffusion_temp[0] << std::endl;
		// }
// 
		// // record x of every particle
		// for (auto const particle_i_ptr : particles_ptr_list_) {
		// 	out_diff4 << std::setw(5) << t << " ";
		// 	auto x_temp = particle_i_ptr->get_position();
		// 	out_diff4 << std::setw(16) << std::setprecision(6) << x_temp[1]
		// 	    << std::endl;
		// }

		// // compute weight and rho at "every" x at t = 0
		// if (std::abs(t-0.) < delta_t/4.) {
		// 	for (double x = -5; x <= 5.; x+=0.01) {
		// 		fourvec x_temp(t, x, 0., 0.);
		// 		std::vector<myreal> weight_temp = compute_weight(x_temp);
		// 		std::vector<myreal> dx_weight_temp = compute_dx_weight(x_temp);
// 
		// 		myreal rho_temp = compute_rho(x_temp);
		// 		out_weight1 << std::setw(5) << x << "  " << std::setw(15) 
		// 		    << std::setprecision(8) << weight_temp[0] << std::endl;
		// 		out_rho1 << std::setw(5) << x << "  " << std::setw(15) 
		// 		    << std::setprecision(8) << rho_temp << std::endl;
		// 	}
		// }
// 
		// // compute weight and rho at "every" x at t = 0.1
		// if (std::abs(t-2.) < delta_t/4.) {
		// 	for (double x = -5; x <= 5.; x+=0.01) {
		// 		fourvec x_temp(t, x, 0., 0.);
		// 		std::vector<myreal> weight_temp = compute_weight(x_temp);
		// 		std::vector<myreal> dx_weight_temp = compute_dx_weight(x_temp);
// 
		// 		myreal rho_temp = compute_rho(x_temp);
		// 		out_weight2 << std::setw(5) << x << "  " << std::setw(15) 
		// 		    << std::setprecision(8) << weight_temp[0] << std::endl;
		// 		out_rho2 << std::setw(5) << x << "  " << std::setw(15) 
		// 		    << std::setprecision(8) << rho_temp << std::endl;
		// 	}
		// }
// 
		// // compute weight and rho at "every" x at t = 0.2
		// if (std::abs(t-4.) < delta_t/4.) {
		// 	for (double x = -5; x <= 5.; x+=0.01) {
		// 		fourvec x_temp(t, x, 0., 0.);
		// 		std::vector<myreal> weight_temp = compute_weight(x_temp);
		// 		std::vector<myreal> dx_weight_temp = compute_dx_weight(x_temp);
// 
		// 		myreal rho_temp = compute_rho(x_temp);
		// 		out_weight3 << std::setw(5) << x << "  " << std::setw(15) 
		// 		    << std::setprecision(8) << weight_temp[0] << std::endl;
		// 		out_rho3 << std::setw(5) << x << "  " << std::setw(15) 
		// 		    << std::setprecision(8) << rho_temp << std::endl;
		// 	}
		// }

		euler_step(delta_t); // calculate one time step of ODE

		// output the time step and update
		std::cout << j << std::endl;
		j++;
	}
}


//******************************************************************************
//******************************************************************************
//******************************************************************************
//******************************************************************************
//******************************************************************************
//******************************************************************************
//******************************************************************************


void SPH_Driver::generate_from_file_ref(std::string SPH_particles_filename) {
	// This function is a reference for generating the initial particle list 
	// from a file
	std::string text_string;
	std::ifstream SPH_particles_file(SPH_particles_filename.c_str());
	particles_ptr_list_.clear();

	if (!SPH_particles_file) {
		std::cout << "ERROR: Can not open SPH strings file: " 
				  << SPH_particles_filename << std::endl;
		exit(1);
	}
	getline(SPH_particles_file, text_string); // read the header
	// now read data
	getline(SPH_particles_file, text_string);
	while (!SPH_particles_file.eof()) {
		myreal mass_temp, x_temp, y_temp, z_temp, vx_temp, vy_temp, vz_temp;
		std::stringstream text_stream(text_string);
		std::shared_ptr<SPH_Particle> new_particle_ptr;

		if (type_ == 0) {
			std::shared_ptr<SPH_Particle> temp_particle_ptr (
				new SPH_Particle_Gaussian);
			new_particle_ptr = std::make_shared<SPH_Particle> (
				SPH_Particle_Gaussian());
		}

		text_stream >> mass_temp >> x_temp >> y_temp >> z_temp >> vx_temp 
					>> vy_temp;
		if (!text_stream.eof()) {
			// read in last element
			text_stream >> vz_temp;
		} else {
			// the file line is too short
			std::cout << "ERROR: the format of file " << SPH_particles_filename
					  << " is wrong." << std::endl;
			exit(1);
		}
		if (!text_stream.eof()) {
			// the file line is too long
			std::cout << "ERROR: the format of file " << SPH_particles_filename
					  << " is wrong." << std::endl;
			exit(1);
		}
		// get the new fourvectors
		fourvec x_new(0., x_temp, y_temp, z_temp);
		fourvec v_new(1., vx_temp, vy_temp, vz_temp);
		// check velocities
		if (v_new.three_norm_sq()>1.) {
			// particles is faster than speed of light
			std::cout << "ERROR: velocity given is faster than speed of light. "
			          << "Given velocity: v_x = " << vx_temp 
			          << ", v_y = " << vy_temp << ", v_z = " << vz_temp << "."
			          << std::endl;
			exit(1);
		}
		// if velocity is good, divide by norm to get four velocity
		fourvec u_new = v_new / v_new.norm();

		// set the new_particle_ptr and add it to particles_ptr_list
		new_particle_ptr->set_mass(mass_temp);
		new_particle_ptr->set_rho(1.);
		new_particle_ptr->set_position(x_new);
		new_particle_ptr->set_velocity(u_new);
		particles_ptr_list_.push_back(new_particle_ptr);

		getline(SPH_particles_file, text_string);
	}
	
	SPH_particles_file.close();
}


std::vector<myreal> SPH_Driver::compute_flux_ref1(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// 
	int weight_size = particle_ptr->get_weight().size();
	std::vector<myreal> result (weight_size, 0.);
	fourvec x = particle_ptr->get_position();
	fourvec v = particle_ptr->get_velocity();

	for (const auto & particle_j_ptr : particles_ptr_list_) {
		if (particle_j_ptr->check_distance(x)) {
			myreal mass_j = particle_j_ptr->get_mass();
			myreal rho_j = particle_j_ptr->get_rho();
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();

			myreal dx_W_ij = particle_j_ptr->dx_W(x);
			myreal dy_W_ij = particle_j_ptr->dy_W(x);
			myreal dz_W_ij = particle_j_ptr->dz_W(x);

			// temporary value in calculation
			myreal value = v[1]*dx_W_ij + v[2]*dy_W_ij + v[3]*dz_W_ij;
			value *= mass_j / rho_j;

			for (int i = 0; i < weight_size; i++) {
				result[i] += value * weight_j[i];
			}
		}
	}

	return(result);
}


std::vector<myreal> SPH_Driver::compute_flux_ref2(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function computes the flux at an arbitrary particle using an 
	// anti-symmetrized scheme.
	// get needed information from given particle
	std::vector<myreal> weight = particle_ptr->get_weight();
	myreal rho = particle_ptr->get_rho();
	fourvec x = particle_ptr->get_position();
	fourvec v = particle_ptr->get_velocity();

	std::vector<myreal> result (weight.size(), 0.); // result has weight.size()

	// loop over particles in neighbor list of given particle_ptr
	for (const auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		// get information from particle j
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx_W_ij = particle_j_ptr.lock()->dx_W(x);
		myreal dy_W_ij = particle_j_ptr.lock()->dy_W(x);
		myreal dz_W_ij = particle_j_ptr.lock()->dz_W(x);

		// calculate v dot grad W
		myreal value = v[1]*dx_W_ij + v[2]*dy_W_ij + v[3]*dz_W_ij;
		value *= mass_j / rho;

		// loop over weight vector and add contribution to result
		for (unsigned int i = 0; i < weight.size(); i++) {
			result[i] += value * (weight_j[i] - weight[i]);
		}
	}

	return(result);
}


myreal SPH_Driver::compute_rho2(const fourvec &x) {
	// This function is a second scheme to compute rho at an arbitrary position
	// x.
	myreal rho = 0.;
	myreal norm = 0.;
	for (const auto & particle_j_ptr : particles_ptr_list_) {
		// check the distance of particle_j to x
		if (particle_j_ptr->check_distance(x)) {
			// if within support of W, add contribution towards rho and norm
			myreal mass_j = particle_j_ptr->get_mass();
			myreal rho_j = particle_j_ptr->get_rho();
			myreal W_ij = particle_j_ptr->W(x);

			rho += mass_j * W_ij;
			norm += mass_j/rho_j * W_ij;
		}
	}

	return(rho/(norm+small_eps));
}


void SPH_Driver::update_rho2() {
	// This fucntion is a second scheme to update rho for all particles. 
	std::vector<myreal> rho (particles_ptr_list_.size(), 0.);
	std::vector<myreal> norm (particles_ptr_list_.size(), 0.);
	for (unsigned int i = 0; i < particles_ptr_list_.size(); i++) {
		std::shared_ptr<SPH_Particle> particle_i_ptr = particles_ptr_list_[i];
		// get particle_i position
		fourvec x_i = particle_i_ptr->get_position(); 

		// loop over particles in neighbor_list of particle_i and add to rho
		for (auto & particle_j_ptr : particle_i_ptr->get_neighbor_list()) {
			myreal mass_j = particle_j_ptr.lock()->get_mass();
			myreal rho_j = particle_j_ptr.lock()->get_rho();
			myreal W_ij = particle_j_ptr.lock()->W(x_i);

			rho[i] += mass_j * W_ij;
			norm[i] += mass_j/rho_j * W_ij;
		}
	}
	for (unsigned int i = 0; i < particles_ptr_list_.size(); i++) {
		particles_ptr_list_[i]->set_rho(rho[i]/(norm[i]+small_eps));
	}
}


std::vector<myreal> SPH_Driver::compute_laplacian1(const fourvec &x) {
	// This function computes the laplacian of phi(x) at an arbitrary position
	// using the basic SPH scheme.
	std::vector<myreal> laplacian; // initialize empty vectorf for laplacian
	int laplacian_size = -1;
	int dim = 3; // dimension of particle for laplacian

	for (const auto & particle_j_ptr : particles_ptr_list_) {
		// for first particle j, get the size of weight vector
		if (laplacian_size == -1) {
			dim = particle_j_ptr->get_dim(); // get dimension of first particle

			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			laplacian_size = weight_j.size();
			// set return weight vector to have length weight_size
			laplacian = std::vector<myreal> (laplacian_size, 0.);
		}
		// check the distance of particle_j to x
		if (particle_j_ptr->check_distance(x)) {
			// if within support of W, add contribution to laplacian
			myreal mass_j = particle_j_ptr->get_mass();
			myreal rho_j = particle_j_ptr->get_rho();
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			myreal dx2_W_ij = particle_j_ptr->dx2_W(x);
			myreal dy2_W_ij = particle_j_ptr->dy2_W(x);
			myreal dz2_W_ij = particle_j_ptr->dz2_W(x);
			myreal lap_W_ij = 0.;

			// calculate laplacian based on dimension
			if (dim == 1) {
				lap_W_ij = dx2_W_ij;
			}
			else if (dim == 2) {
				lap_W_ij = dx2_W_ij + dy2_W_ij;
			}
			else if (dim == 3) {
				lap_W_ij = dx2_W_ij + dy2_W_ij + dz2_W_ij;
			}

			myreal temp = mass_j/rho_j * lap_W_ij; // temporary value

			// loop over weight vector and calculate diffusion source term
			for (int i = 0; i < laplacian_size; i++) {
				laplacian[i] += temp * (weight_j[i]);
			}
		}
	}

	return(laplacian);
}


std::vector<myreal> SPH_Driver::compute_laplacian2(const fourvec &x) {
	// This function computes the laplacian of phi(x) at an arbitrary position
	// using antisymmetrized SPH scheme
	std::vector<myreal> laplacian; // initialize empty vector for laplacian
	int laplacian_size = -1;
	std::vector<myreal> weight = compute_weight(x); // get weight at x

	int dim = 3; // dimension of particle for laplacian

	for (const auto & particle_j_ptr : particles_ptr_list_) {
		// for first particle j, get the size of weight vector
		if (laplacian_size == -1) {
			dim = particle_j_ptr->get_dim(); // get dimension of first particle

			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			laplacian_size = weight_j.size();
			// set return weight vector to have length weight_size
			laplacian = std::vector<myreal> (laplacian_size, 0.);
		}
		// check the distance of particle_j to x
		if (particle_j_ptr->check_distance(x)) {
			// if within support of W, add contribution to laplacian
			myreal mass_j = particle_j_ptr->get_mass();
			myreal rho_j = particle_j_ptr->get_rho();
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			myreal dx2_W_ij = particle_j_ptr->dx2_W(x);
			myreal dy2_W_ij = particle_j_ptr->dy2_W(x);
			myreal dz2_W_ij = particle_j_ptr->dz2_W(x);
			myreal lap_W_ij = 0.;

			// calculate laplacian based on dimension
			if (dim == 1) {
				lap_W_ij = dx2_W_ij;
			}
			else if (dim == 2) {
				lap_W_ij = dx2_W_ij + dy2_W_ij;
			}
			else if (dim == 3) {
				lap_W_ij = dx2_W_ij + dy2_W_ij + dz2_W_ij;
			}

			myreal temp = mass_j/rho_j * lap_W_ij; // temporary value

			// loop over weight vector and calculate diffusion source term
			for (int i = 0; i < laplacian_size; i++) {
				laplacian[i] += temp * (weight_j[i]-weight[i]);
			}
		}
	}

	return(laplacian);
}


std::vector<myreal> SPH_Driver::compute_laplacian3(const fourvec &x) {
	// This function computes the laplacian of phi(x) at an arbitrary position
	// using symmetrized SPH scheme
	std::vector<myreal> laplacian; // initialize empty vector for laplacian
	int laplacian_size = -1;
	std::vector<myreal> weight = compute_weight(x); // get weight at x
	myreal rho = compute_rho(x); // get rho at x

	int dim = 3; // dimension of particle for laplacian
	
	for (const auto & particle_j_ptr : particles_ptr_list_) {
		// for first particle j, get the size of weight vector
		if (laplacian_size == -1) {
			dim = particle_j_ptr->get_dim(); // get dimension of first particle

			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			laplacian_size = weight_j.size();
			// set return weight vector to have length weight_size
			laplacian = std::vector<myreal> (laplacian_size, 0.);
		}
		// check the distance of particle_j to x
		if (particle_j_ptr->check_distance(x)) {
			// if within support of W, add contribution to laplacian
			myreal mass_j = particle_j_ptr->get_mass();
			myreal rho_j = particle_j_ptr->get_rho();
			std::vector<myreal> weight_j = particle_j_ptr->get_weight();
			myreal dx2_W_ij = particle_j_ptr->dx2_W(x);
			myreal dy2_W_ij = particle_j_ptr->dy2_W(x);
			myreal dz2_W_ij = particle_j_ptr->dz2_W(x);
			myreal lap_W_ij = 0.;

			// calculate laplacian based on dimension
			if (dim == 1) {
				lap_W_ij = dx2_W_ij;
			}
			else if (dim == 2) {
				lap_W_ij = dx2_W_ij + dy2_W_ij;
			}
			else if (dim == 3) {
				lap_W_ij = dx2_W_ij + dy2_W_ij + dz2_W_ij;
			}

			myreal temp = mass_j * rho_j * lap_W_ij; // temporary value

			// loop over weight vector and calculate diffusion source term
			for (int i = 0; i < laplacian_size; i++) {
				laplacian[i] += temp * (weight_j[i]/(rho_j*rho_j) 
					+ weight[i]/(rho * rho));
			}
		}
	}

	return(laplacian);
}


std::vector<myreal> SPH_Driver::compute_causal_diff_ref(
            const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function is a reference to compute the causal diffusion source term 
	// at any particle. This function assumes that weight is a 5 dimensional 
	// vector, as follows: weight = (n, q^t, q^x, q^y, q^z)
	// This is derived such that D = -1, which is wrong.
	std::vector<myreal> weight = particle_ptr->get_weight();
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	fourvec x = particle_ptr->get_position();
	fourvec u = particle_ptr->get_velocity();

	myreal gamma = u[0];
	myreal gam_sq = gamma * gamma;
	myreal vx = u[1]/gamma;
	myreal vy = u[2]/gamma;
	myreal vz = u[3]/gamma;

	myreal theta = get_theta(x);
	myreal tau = 1.;

	// constant factor factored out of matrix
	myreal factor = (1. + gam_sq * (tau - 1.));

	// gradient and divergence terms
	for (auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		myreal rho_j = particle_j_ptr.lock()->get_rho();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx_W_ij = particle_j_ptr.lock()->dx_W(x);
		myreal dy_W_ij = particle_j_ptr.lock()->dy_W(x);
		myreal dz_W_ij = particle_j_ptr.lock()->dz_W(x);

		// compute divergence of (q^x, q^y, q^z)
		myreal div_q = weight_j[2] * dx_W_ij + weight_j[3] * dy_W_ij 
			+ weight_j[4] * dz_W_ij;
		// compute contribution of v^k d_k n and v^k d_k q^t
		myreal vkdk = vx * dx_W_ij + vy * dy_W_ij + vz * dz_W_ij;
		myreal vkdk_n = vkdk * weight_j[0];
		myreal vkdk_qt = vkdk * weight_j[1];

		// compute contribution of differential terms
		result[0] += mass_j/rho_j * (vkdk_n + gamma * tau * vkdk_qt 
			- gamma * tau * div_q);
		result[1] += mass_j/rho_j * (- gamma * vkdk_n - (gam_sq - 1.) * vkdk_qt
			+ (gam_sq - 1.) * div_q);
		result[2] += mass_j/rho_j * ((-factor * weight_j[0] * dx_W_ij 
			- gamma * u[1] * vkdk_n)/(gamma * tau) - gamma * u[1] * vkdk_qt
			+ gamma * u[1] * div_q);
		result[3] += mass_j/rho_j * ((-factor * weight_j[0] * dy_W_ij 
			- gamma * u[2] * vkdk_n)/(gamma * tau) - gamma * u[2] * vkdk_qt
			+ gamma * u[2] * div_q);
		result[4] += mass_j/rho_j * ((-factor * weight_j[0] * dz_W_ij 
			- gamma * u[3] * vkdk_n)/(gamma * tau) - gamma * u[3] * vkdk_qt
			+ gamma * u[3] * div_q);
	}

	// non differential terms
	result[0] += -gamma * theta * tau * weight[0] + weight[1];
	result[1] += (gam_sq - 1.) * theta * weight[0] - gamma * weight[1];
	result[2] += gamma * u[1] * theta * weight[0] - u[1] * weight[1]/tau 
		- factor/(gamma * tau) * weight[2];
	result[3] += gamma * u[2] * theta * weight[0] - u[2] * weight[1]/tau 
		- factor/(gamma * tau) * weight[3];
	result[4] += gamma * u[3] * theta * weight[0] - u[3] * weight[1]/tau 
		- factor/(gamma * tau) * weight[4];

	// divide by overall constant factor (1 + gamma^2 * (tau - 1))
	for (int i = 0; i < 5; i++) {
		result[i] = result[i] / factor;
	}

	return(result);
}


std::vector<myreal> SPH_Driver::compute_causal_diff_ref2(
            const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function is a reference to compute the causal diffusion source term 
	// at any particle. This function assumes that weight is a 5 dimensional 
	// vector, as follows: weight = (n, j^t, j^x, j^y, j^z)
	std::vector<myreal> weight = particle_ptr->get_weight();
	std::vector<myreal> result (weight.size(), 0.); // result vector of zeros
	fourvec x = particle_ptr->get_position();

	myreal D = 1.;
	myreal tau = 1.;

	// gradient and divergence terms
	for (auto & particle_j_ptr : particle_ptr->get_neighbor_list()) {
		myreal mass_j = particle_j_ptr.lock()->get_mass();
		myreal rho_j = particle_j_ptr.lock()->get_rho();
		std::vector<myreal> weight_j = particle_j_ptr.lock()->get_weight();
		myreal dx_W_ij = particle_j_ptr.lock()->dx_W(x);
		myreal dy_W_ij = particle_j_ptr.lock()->dy_W(x);
		myreal dz_W_ij = particle_j_ptr.lock()->dz_W(x);

		// compute divergence of (j^x, j^y, j^z)
		myreal div_j = weight_j[2] * dx_W_ij + weight_j[3] * dy_W_ij 
			+ weight_j[4] * dz_W_ij;
		// compute contribution to source term
		result[0] += -mass_j/rho_j * div_j;
		result[2] += -D/tau * mass_j/rho_j * weight_j[0] * dx_W_ij;
		result[3] += -D/tau * mass_j/rho_j * weight_j[0] * dy_W_ij;
		result[4] += -D/tau * mass_j/rho_j * weight_j[0] * dz_W_ij;
	}

	// non differential terms
	result[2] += -1./tau * weight[2];
	result[3] += -1./tau * weight[3];
	result[4] += -1./tau * weight[4];

	//! make sure that result[1] = 0, as it should right now be
	result[1] = 0;

	return(result);
}