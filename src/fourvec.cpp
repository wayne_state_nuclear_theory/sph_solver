//  file: fourvec.cpp
//
//  Header file for the fourvec C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/31/19  Original header file
//
//  Notes:
//
//***************************************************************************

// include files
#include "fourvec.h"
#include <iostream>
#include <cmath>

fourvec::fourvec(const myreal a0, const myreal a1, const myreal a2,
                const myreal a3) {
    // This function constructs the fourvector with given numbers a0, a1, a2, 
    // and a3.
    a_ = {a0, a1, a2, a3};
}


myreal fourvec::norm_sq() const {
    // This function calculates the norm squared of this fourvector
    return a_[0]*a_[0] - a_[1]*a_[1] - a_[2]*a_[2] - a_[3]*a_[3];
}


myreal fourvec::norm() const {
    // This function calculates the norm of this fourvector
    return sqrt(norm_sq());
}


myreal fourvec::scalarproduct(const fourvec &v2)  const {
    // This function calculates the scalar product of this fourvector with 
    // another fourvector v2.
    return a_[0]*v2[0] - a_[1]*v2[1] - a_[2]*v2[2] - a_[3]*v2[3];
}


myreal fourvec::three_norm_sq() const {
    // This function calculates the norm squared of the usual 3dim-vector
    return a_[1]*a_[1] + a_[2]*a_[2] + a_[3]*a_[3];
}


myreal fourvec::three_norm() const {
    // This function calculates the norm of the usual 3dim-vector
    return sqrt(three_norm_sq());
}


myreal fourvec::three_scalarproduct(const fourvec &v2) const {
    // This function calculates the three dim scalar product of this fourvector
    // with another fourvector v2.
    return a_[1]*v2[1] + a_[2]*v2[2] + a_[3]*v2[3];
}


myreal fourvec::operator[](const int i) const {
    // This function overloads the access operator at index i for a fourvector
    int n = a_.size();
    if ((i >= n) || (i < -n)) {
        std::cout << "ERROR: index out of range. index is " << i
            << ". Size of vector is " << n << "." << std::endl;
        exit(0);
    }

    return(a_[(n+i)%n]);
}


fourvec fourvec::operator +(const fourvec &v2) const {
    // This function overloads the addition operator between two fourvectors
    fourvec result(0.,0.,0.,0.);

    for (unsigned int i = 0; i < a_.size(); i++) {
        result.set_a_i(i, a_[i]+v2[i]);
    }
    
    return(result);
}


fourvec fourvec::operator -(const fourvec &v2) const {
    // This function overloads the subtraction operator between two fourvectors
    fourvec result(0.,0.,0.,0.);

    for (unsigned int i = 0; i < a_.size(); i++) {
        result.set_a_i(i, a_[i]-v2[i]);
    }
    
    return(result);
}


fourvec fourvec::operator *(const myreal c) const {
    // This function overloads the multiplication operator to multiply a 
    // fourvector by a constant
    fourvec result(0.,0.,0.,0.);

    for (unsigned int i = 0; i < a_.size(); i++) {
        result.set_a_i(i, c*a_[i]);
    }
    
    return(result);
}


fourvec fourvec::operator /(const myreal c) const {
    // This function overloads the multiplication operator to divide a 
    // fourvector by a constant
    fourvec result(0.,0.,0.,0.);

    for (unsigned int i = 0; i < a_.size(); i++) {
        result.set_a_i(i, a_[i]/c);
    }
    
    return(result);
}
 

// fourvec & fourvec::operator =(const fourvec &v2) {
//     a_ = v2.get_a();
//     
//     return(*this);
// }
// 
