//  file: SPH_Cell.cpp
//
//  Definitions for the SPH_Cell C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      06/18/19  Original file
//
//  Notes:
//
//***************************************************************************

// include files
#include "SPH_Cell.h"

void SPH_Cell::add_to_linked_list(
	const std::shared_ptr<SPH_Particle> particle_ptr) {
	// This function adds a pointer to a particle to the linked_particles_list 
	// of this cell
	std::weak_ptr<SPH_Particle> temp_ptr = particle_ptr;
	linked_list_.push_back(temp_ptr);
}

std::vector<std::weak_ptr<SPH_Particle>> SPH_Cell::get_linked_list(
	) const {
	// This fucntion returns linked_particles_list
	return(linked_list_);
}