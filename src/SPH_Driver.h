//  file: SPH_Driver.h
//
//  Header file for the SPH_Driver C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/31/19  Original header file
//
//  Notes:
//
//***************************************************************************

// The ifndef/define macro ensures that the header is only included once
#ifndef SPH_DRIVER_H
#define SPH_DRIVER_H

// include files
#include "SPH_Particle.h"
#include "SPH_Particle_Gaussian.h"
#include "SPH_Mesh.h"
#include "Random.h"
#include <string>
#include <memory>

class SPH_Driver { 
    private:
        std::vector<std::shared_ptr<SPH_Particle>> particles_ptr_list_;
        int type_; // 0 = Gaussian.
        SPH_Mesh mesh_;

    public:
        SPH_Driver(const int num_particle, const int type); // Constructor
        ~SPH_Driver();  // Destructor

        // accessor functions
        std::vector<std::shared_ptr<SPH_Particle>> get_particles_ptr_list(
            ) const;

        // functions to generate the initial particle list
        virtual void generate_init_particles();
        void generate_from_file(std::string SPH_particles_filename);

        // generate mesh
        void generate_mesh(const myreal size_x, const myreal size_y, 
            const myreal size_z, const myreal dx, const myreal dy, 
            const myreal dz);
        // update neighboring particles
        void update_neighboring_particles();
        // update neighboring particles
        void update_neighboring_particles_ref();

        // update position and velocity
        void update_position(const myreal delta_t);
        virtual void update_velocity();

        // compute density rho at any position x
        myreal compute_rho(const fourvec &x);
        // update rho for all particle_i in particles_ptr_list_
        void update_rho();

        // evolve system for time delta_t
        void evolve_system(const myreal delta_t);

        // get velocity from the background at a certain position
        fourvec get_u_background(const fourvec &x);
        // get theta, the expansion rate, at a certain position
        myreal get_theta(const fourvec &x);
        // get shear viscosity at a certain position
        myreal get_shear_viscosity(const fourvec &x);
        // get speed of sound squared, c_s^2, of fluid at a certain position
        myreal get_c_s_sq(const fourvec &x);
        // get speed of sound, c_s, of fluid at a certain position
        myreal get_c_s(const fourvec &x);

        // compute weight at any position x
        std::vector<myreal> compute_weight(const fourvec &x);
        // compute dx_weight at any position x
        std::vector<myreal> compute_dx_weight(const fourvec &x);
        // compute equilibrium weight at any particle
        virtual std::vector<myreal> compute_eq_weight(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        // compute relaxation term at any particle
        virtual std::vector<myreal> compute_relax_source(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        // compute matrix source term at any particle
        virtual std::vector<myreal> compute_matrix_source(
            const std::shared_ptr<SPH_Particle> particle_ptr);

        // compute source term from diffusion at any particle
        virtual std::vector<myreal> compute_diffusion1(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        virtual std::vector<myreal> compute_diffusion2(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        virtual std::vector<myreal> compute_diffusion3(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        // compute causal diff source term at any particle
        virtual std::vector<myreal> compute_causal_diff(
            const std::shared_ptr<SPH_Particle> particle_ptr);

        // compute one Euler time step
        void euler_step(const myreal delta_t);
        // compute one generalized RK_2 time step
        void generalized_RK_2(const myreal delta_t, const myreal alpha);
        // compute one RK_4 time step
        void RK_4(const myreal delta_t);
        // solve the ODE for a period of time
        void ODE_solver(const myreal t_init, const myreal t_final,
                        const myreal delta_t);

        //*********************************************************************
        //! reference for generate_from file
        void generate_from_file_ref(std::string SPH_particles_filename);
        //! reference for computing the flux term at a particle
        std::vector<myreal> compute_flux_ref1(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        //! second reference for computing the flux term at a particle using
        //! antisymmetrazion method
        std::vector<myreal> compute_flux_ref2(
            const std::shared_ptr<SPH_Particle> particle_ptr);

        //! compute density rho at any position x
        myreal compute_rho2(const fourvec &x);
        //! update rho for all particle_i in particles_ptr_list_
        void update_rho2();

        //! compute laplacian at any particle
        virtual std::vector<myreal> compute_laplacian1(const fourvec &x);
        virtual std::vector<myreal> compute_laplacian2(const fourvec &x);
        virtual std::vector<myreal> compute_laplacian3(const fourvec &x);

        //! reference for computing causal diff source term at any particle
        virtual std::vector<myreal> compute_causal_diff_ref(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        //! reference for computing causal diff source term at any particle
        virtual std::vector<myreal> compute_causal_diff_ref2(
            const std::shared_ptr<SPH_Particle> particle_ptr);
};

#endif  // SPH_DRIVER_H