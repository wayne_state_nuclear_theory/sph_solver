//  file: SPH_Mesh.cpp
//
//  Definitions for the SPH_Mesh C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      06/18/19  Original file
//
//  Notes:
//
//***************************************************************************

// include files
#include "SPH_Mesh.h"
#include <iostream>
#include <cassert>

SPH_Mesh::SPH_Mesh(const myreal size_x, const myreal size_y, const myreal size_z, 
	const myreal dx, const myreal dy, const myreal dz) {
	// This function constructs a mesh given a box of size size_x, size_y, and
	// size_z, with spacing dx, dy, and dz.
	size_x_ = size_x;
	size_y_ = size_y;
	size_z_ = size_z;

	dx_ = dx;
	dy_ = dy;
	dz_ = dz;

	// find the number of points in each direction
	Nx_ = static_cast<int>(size_x_/dx_) + 1;
	Ny_ = static_cast<int>(size_y_/dy_) + 1;
	Nz_ = static_cast<int>(size_z_/dz_) + 1;

	// find the total number of points
	int total_pts = Nx_ * Ny_ * Nz_;

	// initialze the mesh_ to have total_pts SPH_Cell's
	for (int i = 0; i < total_pts; i++) {
		mesh_.push_back(SPH_Cell());
	}
}


SPH_Mesh::SPH_Mesh(){
	// This function is the default constructor for a mesh. Set everything to 0
	size_x_ = 0.;
	size_y_ = 0.;
	size_z_ = 0.;

	dx_ = 0.;
	dy_ = 0.;
	dz_ = 0.;

	Nx_ = 0;
	Ny_ = 0;
	Nz_ = 0;

	// keep mesh vector empty
}


SPH_Cell& SPH_Mesh::get_cell(const int idx_x, const int idx_y, const int idx_z){
	// This function returns the cell given a vector of indices in each 
	// direction
	// make sure indices are in the mesh
	assert(0 <= idx_x); assert(idx_x < Nx_);
	assert(0 <= idx_y); assert(idx_y < Ny_);
	assert(0 <= idx_z); assert(idx_z < Nz_);

	if((idx_x < 0) || (idx_x >= Nx_)) {
		std::cout << "ERROR: idx_x is out of range. idx_x = " << idx_x
			<< ", while Nx_ = " << Nx_ << std::endl;
		exit(0);
	}
	if((idx_y < 0) || (idx_y >= Ny_)) {
		std::cout << "ERROR: idx_y is out of range. idx_y = " << idx_y
			<< ", while Ny_ = " << Ny_ << std::endl;
		exit(0);
	}
	if((idx_z < 0) || (idx_z >= Nz_)) {
		std::cout << "ERROR: idx_z is out of range. idx_z = " << idx_z
			<< ", while Nz_ = " << Nz_ << std::endl;
		exit(0);
	}

	// find the index of the cell: Nx_(Ny_*idx_z + idx_y) + idx_x.
	int cell_index = Nx_*(Ny_*idx_z + idx_y) + idx_x;

	// return wanted cell
	return(mesh_[cell_index]);
}


void SPH_Mesh::update_linked_list(
	std::vector<std::shared_ptr<SPH_Particle>> particle_list) {
	// This function updates the linked_particles_list of each cell with 
	// the particles given
	// clear the linked particles list for all particles
	for (auto & cell_i : mesh_) {
		cell_i.clear_linked_list();
	}

	// loop over particles
	for (const auto & particle_i : particle_list) {
		fourvec x = particle_i->get_position();  // get position of particle_i
		std::vector<int> temp_indices; // vector of indices

		// find indices in each direction
		int idx_x = static_cast<int>((x[1]-(-size_x_/2.))/dx_);
		int idx_y = static_cast<int>((x[2]-(-size_y_/2.))/dy_);
		int idx_z = static_cast<int>((x[3]-(-size_z_/2.))/dz_);

		// get cell at given indices, and push particle into linked_list
		get_cell(idx_x, idx_y, idx_z).add_to_linked_list(particle_i);
	}
}