#include "doctest.h"
#include "SPH_Driver.h"
#include <cmath>

using doctest::Approx;

TEST_CASE("Check generate_from_file and get_particles_ptr_list") {
	// initialize driver
	SPH_Driver testdriver(0, 0);
	// read in from file
	testdriver.generate_from_file("test/test_particles_weights.txt");

	// get particle list and check that there are 3 particles
	auto test_list = testdriver.get_particles_ptr_list();
	auto check_v = test_list.size();
	CHECK(check_v == 3);

	// 1st particle, check all private variables
	auto check_v1 = test_list[0]->get_mass();
	CHECK(check_v1 == Approx(1.));

	check_v1 = test_list[0]->get_rho();
	CHECK(check_v1 == Approx(1.));

	auto check_v2 = test_list[0]->get_position();
	CHECK(check_v2[0] == Approx(0.));
	CHECK(check_v2[1] == Approx(0.));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	check_v2 = test_list[0]->get_velocity();
	CHECK(check_v2[0] == Approx(std::sqrt(4./3.)));
	CHECK(check_v2[1] == Approx(1./std::sqrt(3.)));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	auto check_v3 = test_list[0]->get_weight();
	CHECK(check_v3.size() == 2);
	CHECK(check_v3[0] == 1.);
	CHECK(check_v3[1] == 1.);

	check_v1 = test_list[0]->get_h();
	CHECK(check_v1 == Approx(0.1));

	// 2nd particle, check all private variables
	check_v1 = test_list[1]->get_mass();
	CHECK(check_v1 == Approx(1.));

	check_v1 = test_list[1]->get_rho();
	CHECK(check_v1 == Approx(1.));

	check_v2 = test_list[1]->get_position();
	CHECK(check_v2[0] == Approx(0.));
	CHECK(check_v2[1] == Approx(0.));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	check_v2 = test_list[1]->get_velocity();
	CHECK(check_v2[0] == Approx(std::sqrt(4./3.)));
	CHECK(check_v2[1] == Approx(1./std::sqrt(3.)));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	check_v3 = test_list[1]->get_weight();
	CHECK(check_v3.size() == 2);
	CHECK(check_v3[0] == 1.);
	CHECK(check_v3[1] == 2.);

	check_v1 = test_list[1]->get_h();
	CHECK(check_v1 == Approx(0.1));

	// 3rd particle, check all private variables
	check_v1 = test_list[2]->get_mass();
	CHECK(check_v1 == Approx(1.));

	check_v1 = test_list[2]->get_rho();
	CHECK(check_v1 == Approx(1.));

	check_v2 = test_list[2]->get_position();
	CHECK(check_v2[0] == Approx(0.));
	CHECK(check_v2[1] == Approx(-1.));
	CHECK(check_v2[2] == Approx(-1.));
	CHECK(check_v2[3] == Approx(-1.));

	check_v2 = test_list[2]->get_velocity();
	CHECK(check_v2[0] == Approx(std::sqrt(4./3.)));
	CHECK(check_v2[1] == Approx(-1./std::sqrt(3.)));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	check_v3 = test_list[2]->get_weight();
	CHECK(check_v3.size() == 2);
	CHECK(check_v3[0] == 2.);
	CHECK(check_v3[1] == 1.);

	check_v1 = test_list[2]->get_h();
	CHECK(check_v1 == Approx(0.1));
}

TEST_CASE("Check update_neighboring_particles") {
	// initialize driver
	SPH_Driver testdriver(0, 0);
	// read in from file
	testdriver.generate_from_file("test/test_particles_weights.txt");
	
	// get particles list and update neighboring particles
	auto test_list = testdriver.get_particles_ptr_list();
	testdriver.update_neighboring_particles_ref();

	// check particle_1's neighbor_list
	auto check_v = test_list[0]->get_neighbor_list();
	CHECK(check_v.size() == 2);
	CHECK(check_v[0].lock() == test_list[0]);
	CHECK(check_v[1].lock() == test_list[1]);
	
	// check particle_2's neighbor_list
	check_v = test_list[1]->get_neighbor_list();
	CHECK(check_v.size() == 2);
	CHECK(check_v[0].lock() == test_list[0]);
	CHECK(check_v[1].lock() == test_list[1]);

	// check particle_3's neighbor_list
	check_v = test_list[2]->get_neighbor_list();
	CHECK(check_v.size() == 1);
	CHECK(check_v[0].lock() == test_list[2]);

}

TEST_CASE("Check update_position") {
	// initialize driver
	SPH_Driver testdriver(0, 0);
	// read in from file
	testdriver.generate_from_file("test/test_particles_weights.txt");
	
	testdriver.update_position(0.1); // update position of particles
	// get particle list
	auto test_list = testdriver.get_particles_ptr_list();

	// 1st particle, check that position is updated position
	auto check_v2 = test_list[0]->get_position();
	CHECK(check_v2[0] == Approx(0.1));
	CHECK(check_v2[1] == Approx(0.05));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	check_v2 = test_list[0]->get_velocity();
	CHECK(check_v2[0] == Approx(std::sqrt(4./3.)));
	CHECK(check_v2[1] == Approx(1./std::sqrt(3.)));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	// 2nd particle, check that position is updated position
	check_v2 = test_list[1]->get_position();
	CHECK(check_v2[0] == Approx(0.1));
	CHECK(check_v2[1] == Approx(0.05));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	check_v2 = test_list[1]->get_velocity();
	CHECK(check_v2[0] == Approx(std::sqrt(4./3.)));
	CHECK(check_v2[1] == Approx(1./std::sqrt(3.)));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));

	// 3rd particle, check that position is updated position
	check_v2 = test_list[2]->get_position();
	CHECK(check_v2[0] == Approx(0.1));
	CHECK(check_v2[1] == Approx(-1.05));
	CHECK(check_v2[2] == Approx(-1.));
	CHECK(check_v2[3] == Approx(-1.));

	check_v2 = test_list[2]->get_velocity();
	CHECK(check_v2[0] == Approx(std::sqrt(4./3.)));
	CHECK(check_v2[1] == Approx(-1./std::sqrt(3.)));
	CHECK(check_v2[2] == Approx(0.));
	CHECK(check_v2[3] == Approx(0.));
}

// TEST_CASE("Check update_velocity") {
// 	SPH_Driver testdriver(0);
// 	testdriver.generate_from_file("test/test_particles.txt");
// 	
// 	testdriver.update_velocity();
// 	std::vector<SPH_Particle> test_list = testdriver.get_particles();
// 
// 	// 1st particle
// 	auto check_v2 = test_list[0].get_velocity();
// 	// CHECK(check_v2[0] == Approx(1.));  // at rest
// 	// CHECK(check_v2[1] == Approx(0.));
// 	CHECK(check_v2[0] == Approx(2.));  // const x velocity
// 	CHECK(check_v2[1] == Approx(sqrt(3.)));
// 	CHECK(check_v2[2] == Approx(0.));
// 	CHECK(check_v2[3] == Approx(0.));
// 
// 	// 2nd particle
// 	check_v2 = test_list[1].get_velocity();
// 	// CHECK(check_v2[0] == Approx(1.));  // at rest
// 	// CHECK(check_v2[1] == Approx(0.));
// 	CHECK(check_v2[0] == Approx(2.));  // const x velocity
// 	CHECK(check_v2[1] == Approx(sqrt(3.)));
// 	CHECK(check_v2[2] == Approx(0.));
// 	CHECK(check_v2[3] == Approx(0.));
// 
// 	// 3rd particle
// 	check_v2 = test_list[2].get_velocity();
// 	// CHECK(check_v2[0] == Approx(1.));  // at rest
// 	// CHECK(check_v2[1] == Approx(0.));
// 	CHECK(check_v2[0] == Approx(2.));  // const x velocity
// 	CHECK(check_v2[1] == Approx(sqrt(3.)));
// 	CHECK(check_v2[2] == Approx(0.));
// 	CHECK(check_v2[3] == Approx(0.));
// }
 
TEST_CASE("Check compute_rho") {
	// initialize driver
	SPH_Driver testdriver(0, 0);
	// read in from file
	testdriver.generate_from_file("test/test_particles_weights.txt");
	// get particle list
	auto test_list = testdriver.get_particles_ptr_list();

	// check that it computes rho properly at position of particle 1
	fourvec r = test_list[0]->get_position();
	auto check_v = testdriver.compute_rho(r);
	auto check_v2 = test_list[0]->W(r);
	CHECK(check_v == Approx(2*check_v2));

	// check that it computes rho properly at position of particle 2
	r = test_list[1]->get_position();
	check_v = testdriver.compute_rho(r);
	check_v2 = test_list[1]->W(r);
	CHECK(check_v == Approx(2*check_v2));

	// check that it computes rho properly at position of particle 3
	r = test_list[2]->get_position();
	check_v = testdriver.compute_rho(r);
	check_v2 = test_list[2]->W(r);
	CHECK(check_v == Approx(check_v2));
}

TEST_CASE("Check update_rho") {
	// initialize driver
	SPH_Driver testdriver(0, 0);
	// read in from file
	testdriver.generate_from_file("test/test_particles_weights.txt");
	// get particle list
	auto test_list = testdriver.get_particles_ptr_list();

	// compute rho at each particle
	fourvec r1 = test_list[0]->get_position();
	fourvec r2 = test_list[1]->get_position();
	fourvec r3 = test_list[2]->get_position();
	auto check_v1 = testdriver.compute_rho(r1);
	auto check_v2 = testdriver.compute_rho(r2);
	auto check_v3 = testdriver.compute_rho(r3);

	// update rho and check that it updates correctly
	testdriver.update_neighboring_particles_ref();
	testdriver.update_rho();
	test_list = testdriver.get_particles_ptr_list();
	CHECK(check_v1 == Approx(test_list[0]->get_rho()));
	CHECK(check_v2 == Approx(test_list[1]->get_rho()));
	CHECK(check_v3 == Approx(test_list[2]->get_rho()));
}

TEST_CASE("Check compute_weight") {
	// initialize driver
	SPH_Driver testdriver(0, 0);
	// read in from file
	testdriver.generate_from_file("test/test_particles_weights.txt");
	// get particle list
	auto test_list = testdriver.get_particles_ptr_list();

	// compute weight at particle 1 (which is also at particle 2)
	fourvec r = test_list[0]->get_position();
	auto check_v = testdriver.compute_weight(r);
	CHECK(check_v.size() == 2.);

	auto W_1 = test_list[0]->W(r);
	auto rho_1 = test_list[0]->get_rho();
	auto mass_1 = test_list[0]->get_mass();
	auto weight_1 = test_list[0]->get_weight();

	auto W_2 = test_list[1]->W(r);
	auto rho_2 = test_list[1]->get_rho();
	auto mass_2 = test_list[1]->get_mass();
	auto weight_2 = test_list[1]->get_weight();

	// check that weight[0] is computed correctly
	auto result_0 = mass_1 / rho_1 * weight_1[0] * W_1 
		+ mass_2 / rho_2 * weight_2[0] * W_2;
	CHECK(check_v[0] == result_0);

	// check that weight[1] is computed correctly
	auto result_1 = mass_1 / rho_1 * weight_1[1] * W_1 
		+ mass_2 / rho_2 * weight_2[1] * W_2;
	CHECK(check_v[1] == result_1);

	// compute weight at particle 3
	r = test_list[2]->get_position();
	check_v = testdriver.compute_weight(r);
	CHECK(check_v.size() == 2.);

	auto W_3 = test_list[2]->W(r);
	auto rho_3 = test_list[2]->get_rho();
	auto mass_3 = test_list[2]->get_mass();
	auto weight_3 = test_list[2]->get_weight();

	result_0 = mass_3/rho_3 * weight_3[0] * W_3;
	CHECK(check_v[0] == result_0);

	result_1 = mass_3/rho_3 * weight_3[1] * W_3;
	CHECK(check_v[1] == result_1);

	// compute weight far away, and check that you get 0.
	r = fourvec(0., -20., -20., -20.);
	check_v = testdriver.compute_weight(r);
	CHECK(check_v.size() == 2.);
	CHECK(check_v[0] == Approx(0.));
	CHECK(check_v[1] == Approx(0.));
}

TEST_CASE("Check compute_dx_weight") {
	// initialize driver
	SPH_Driver testdriver(0, 0);
	// read in from file
	testdriver.generate_from_file("test/test_particles_weights.txt");
	// get particle list
	auto test_list = testdriver.get_particles_ptr_list();

	// compute dx_weight at particle 1 (which is also at particle 2)
	fourvec r = test_list[0]->get_position();
	auto check_v = testdriver.compute_dx_weight(r);
	CHECK(check_v.size() == 2.);

	auto W_1 = test_list[0]->dx_W(r);
	auto rho_1 = test_list[0]->get_rho();
	auto mass_1 = test_list[0]->get_mass();
	auto weight_1 = test_list[0]->get_weight();

	auto W_2 = test_list[1]->dx_W(r);
	auto rho_2 = test_list[1]->get_rho();
	auto mass_2 = test_list[1]->get_mass();
	auto weight_2 = test_list[1]->get_weight();

	// check that dx_weight[0] is computed correctly
	auto result_0 = mass_1 / rho_1 * weight_1[0] * W_1 
		+ mass_2 / rho_2 * weight_2[0] * W_2;
	CHECK(check_v[0] == result_0);

	// check that dx_weight[1] is computed correctly
	auto result_1 = mass_1 / rho_1 * weight_1[1] * W_1 
		+ mass_2 / rho_2 * weight_2[1] * W_2;
	CHECK(check_v[1] == result_1);

	// compute dx_weight at particle 3
	r = test_list[2]->get_position();
	check_v = testdriver.compute_dx_weight(r);
	CHECK(check_v.size() == 2.);

	auto W_3 = test_list[2]->dx_W(r);
	auto rho_3 = test_list[2]->get_rho();
	auto mass_3 = test_list[2]->get_mass();
	auto weight_3 = test_list[2]->get_weight();

	result_0 = mass_3/rho_3 * weight_3[0] * W_3;
	CHECK(check_v[0] == result_0);

	result_1 = mass_3/rho_3 * weight_3[1] * W_3;
	CHECK(check_v[1] == result_1);

	// compute dx_weight far away, and check that you get 0.
	r = fourvec(0., -20., -20., -20.);
	check_v = testdriver.compute_dx_weight(r);
	CHECK(check_v.size() == 2.);
	CHECK(check_v[0] == Approx(0.));
	CHECK(check_v[1] == Approx(0.));
}