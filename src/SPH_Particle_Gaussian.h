//  file: SPH_Particle_Guassian.h
//
//  Header file for the SPH_Particle_Gaussian C++ derived class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      06/17/19  Original header file
//
//  Notes:
//
//***************************************************************************

// The ifndef/define macro ensures that the header is only included once
#ifndef SPH_PARTICLE_GAUSSIAN_H
#define SPH_PARTICLE_GAUSSIAN_H

// include files
#include "SPH_Particle.h"

class SPH_Particle_Gaussian : public SPH_Particle { 
    private:
        const myreal kappa_; // coefficient of h_ for support of W
                             // default = 5. for Gaussian
        myreal norm_const_; // normalization constant for Gaussian dependent on 
                      // dimension

    public:
        SPH_Particle_Gaussian(const myreal mass, const myreal rho, 
                              const std::vector<myreal> &weight, 
                              const fourvec &x, const fourvec &u,
                              const myreal h, const int dim);  // Constructor
        SPH_Particle_Gaussian(); // Defualt constructor
        ~SPH_Particle_Gaussian(); // Destructor

        // accessor functions
        void set_h(const myreal h);
        void set_dim(const int dim);
        myreal get_norm_const() const {return(norm_const_);}

        // smoothing functions and derivatives
        myreal W(const fourvec &x) const;
        myreal dr_W(const fourvec &x) const;
        myreal dr2_W(const fourvec &x) const;

        myreal dx2_W(const fourvec &x) const;
        myreal dy2_W(const fourvec &x) const;
        myreal dz2_W(const fourvec &x) const;
};

#endif  // SPH_Particle_Guassian_H
