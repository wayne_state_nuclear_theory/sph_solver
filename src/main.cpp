//  file: main.cpp
//
//  Random test for SPH_Particle C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/30/19  Original file obtained form SPH_Particle.cpp
//
//  Notes:
//
//***************************************************************************

// include files
#include "SPH_Particle.h"  // include the SPH Particle class header file
#include "SPH_Driver.h"  // include the SPH Driver class header file
#include <iostream>

int main()  {
	// make a driver with 500 particles of type Gaussian
    SPH_Driver test_driver(61, 0);
    // generate the initial particles
    test_driver.generate_init_particles();
    // solve the ODE
    test_driver.ODE_solver(0., 0.005, 0.01);

    return(0);
}
