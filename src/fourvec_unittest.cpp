#include "doctest.h"
#include "fourvec.h"
#include <cmath>

using doctest::Approx;

TEST_CASE("Check set_a and get_a") {
    // initialize a fourvector
    std::array<double, 4> a_init = {1., 2., 3., 4.};
    fourvec testfourvec(a_init[0], a_init[1], a_init[2], a_init[3]);
    fourvec testfourvec2 = testfourvec; // check equalizing operator

    // check fourvector components of testfourvec2
    auto check_v = testfourvec2.get_a();
    CHECK(check_v[0] == Approx(a_init[0]));
    CHECK(check_v[1] == Approx(a_init[1]));
    CHECK(check_v[2] == Approx(a_init[2]));
    CHECK(check_v[3] == Approx(a_init[3]));

    // check another way to initialize fourvector
    fourvec testfourvec3 = {a_init[0], a_init[1], a_init[2], a_init[3]};
    check_v = testfourvec3.get_a();
    CHECK(check_v[0] == Approx(a_init[0]));
    CHECK(check_v[1] == Approx(a_init[1]));
    CHECK(check_v[2] == Approx(a_init[2]));
    CHECK(check_v[3] == Approx(a_init[3]));

    // check set_a to set a new array
    std::array<double, 4> a_new = {1.1, 2.1, 3.1, 4.1};
    testfourvec.set_a(a_new);
    check_v = testfourvec.get_a();
    CHECK(check_v[0] == Approx(a_new[0]));
    CHECK(check_v[1] == Approx(a_new[1]));
    CHECK(check_v[2] == Approx(a_new[2]));
    CHECK(check_v[3] == Approx(a_new[3]));
}

TEST_CASE("Check set_a_i and operator []") {
    // initiailze a fourvector
    fourvec testfourvec(1., 2., 3., 4.);
    // set each element of fourvector separately
    std::array<double, 4> a_new = {1.1, 2.1, 3.1, 4.1};
    testfourvec.set_a_i(0, a_new[0]);
    testfourvec.set_a_i(1, a_new[1]);
    testfourvec.set_a_i(2, a_new[2]);
    testfourvec.set_a_i(3, a_new[3]);

    // check each index of fourvector (both at positive and negative indices)
    auto check_v = testfourvec[0];
    CHECK(check_v == Approx(a_new[0]));
    check_v = testfourvec[-4];
    CHECK(check_v == Approx(a_new[0]));

    check_v = testfourvec[1];
    CHECK(check_v == Approx(a_new[1]));
    check_v = testfourvec[-3];
    CHECK(check_v == Approx(a_new[1]));

    check_v = testfourvec[2];
    CHECK(check_v == Approx(a_new[2]));
    check_v = testfourvec[-2];
    CHECK(check_v == Approx(a_new[2]));

    check_v = testfourvec[3];
    CHECK(check_v == Approx(a_new[3]));
    check_v = testfourvec[-1];
    CHECK(check_v == Approx(a_new[3]));
}

TEST_CASE("Check = operator") {
    // initialize three fourvectors
    std::array<double, 4> a1 = {0., 0., 0., 0.};
    std::array<double, 4> a2 = {1., 2., 3., 4.};
    std::array<double, 4> a3 = {1.1, 2.1, 3.1, 4.1};

    fourvec fourvec1(a1[0], a1[1], a1[2], a1[3]);
    fourvec fourvec2(a2[0], a2[1], a2[2], a2[3]);
    fourvec fourvec3(a3[0], a3[1], a3[2], a3[3]);

    // use assignment operator
    fourvec1 = fourvec2 = fourvec3;
    // check that fourvec1 = fourvec3
    auto check_v = fourvec1.get_a();
    CHECK(check_v[0] == Approx(a3[0]));
    CHECK(check_v[1] == Approx(a3[1]));
    CHECK(check_v[2] == Approx(a3[2]));
    CHECK(check_v[3] == Approx(a3[3]));

    // check that fourvec2 = fourvec3
    check_v = fourvec2.get_a();
    CHECK(check_v[0] == Approx(a3[0]));
    CHECK(check_v[1] == Approx(a3[1]));
    CHECK(check_v[2] == Approx(a3[2]));
    CHECK(check_v[3] == Approx(a3[3]));
}

TEST_CASE("Check + and - operator") {
    // initialize four fourvectors
    std::array<double, 4> a1 = {0., 0., 0., 0.};
    std::array<double, 4> a2 = {1., 2., 3., 4.};
    std::array<double, 4> a3 = {1.1, 2.1, 3.1, 4.1};
    std::array<double, 4> a4 = {1., 0., 0., 0.};

    fourvec fourvec1(a1[0], a1[1], a1[2], a1[3]);
    fourvec fourvec2(a2[0], a2[1], a2[2], a2[3]);
    fourvec fourvec3(a3[0], a3[1], a3[2], a3[3]);
    fourvec fourvec4(a4[0], a4[1], a4[2], a4[3]);

    // check addition and subtraction
    fourvec1 = fourvec2 + fourvec3 - fourvec4;
    auto check_v = fourvec1.get_a();
    CHECK(check_v[0] == Approx(a2[0] + a3[0] - a4[0]));
    CHECK(check_v[1] == Approx(a2[1] + a3[1] - a4[1]));
    CHECK(check_v[2] == Approx(a2[2] + a3[2] - a4[2]));
    CHECK(check_v[3] == Approx(a2[3] + a3[3] - a4[3]));
}

TEST_CASE("Check * operator") {
    // initialize two fourvectors
    std::array<double, 4> a1 = {0., 0., 0., 0.};
    std::array<double, 4> a2 = {1., 2., 3., 4.};

    fourvec fourvec1(a1[0], a1[1], a1[2], a1[3]);
    fourvec fourvec2(a2[0], a2[1], a2[2], a2[3]);
    double constant = 2.; // initialize a constant

    // check multiplication by constant
    fourvec1 = fourvec2 * constant;
    auto check_v = fourvec1.get_a();
    CHECK(check_v[0] == Approx(a2[0]*constant));
    CHECK(check_v[1] == Approx(a2[1]*constant));
    CHECK(check_v[2] == Approx(a2[2]*constant));
    CHECK(check_v[3] == Approx(a2[3]*constant));
}

TEST_CASE("Check / operator") {
    // initialize two fourvectors
    std::array<double, 4> a1 = {0., 0., 0., 0.};
    std::array<double, 4> a2 = {1., 2., 3., 4.};

    fourvec fourvec1(a1[0], a1[1], a1[2], a1[3]);
    fourvec fourvec2(a2[0], a2[1], a2[2], a2[3]);
    double constant = 2.; // initialize a constant

    // check division by constant
    fourvec1 = fourvec2 / constant;
    auto check_v = fourvec1.get_a();
    CHECK(check_v[0] == Approx(a2[0]/constant));
    CHECK(check_v[1] == Approx(a2[1]/constant));
    CHECK(check_v[2] == Approx(a2[2]/constant));
    CHECK(check_v[3] == Approx(a2[3]/constant));
}

TEST_CASE("Check norm and scalarproduct") {
    // initialize two fourvectors
    fourvec fourvec1(3., 1., -1., 2.);
    fourvec fourvec2(2., -2., 3., 1.);

    // check various norm, norm_sq, and scalar product functions
    auto check_v = fourvec1.norm_sq();
    CHECK(check_v == Approx(3.));

    check_v = fourvec1.norm();
    CHECK(check_v == Approx(std::sqrt(3.)));

    check_v = fourvec1.scalarproduct(fourvec2);
    CHECK(check_v == Approx(9.));

    check_v = fourvec2.scalarproduct(fourvec1);
    CHECK(check_v == Approx(9.));

    check_v = fourvec1.three_norm_sq();
    CHECK(check_v == Approx(6.));

    check_v = fourvec1.three_norm();
    CHECK(check_v == Approx(std::sqrt(6.)));

    check_v = fourvec1.three_scalarproduct(fourvec2);
    CHECK(check_v == Approx(-3.));

    check_v = fourvec2.three_scalarproduct(fourvec1);
    CHECK(check_v == Approx(-3.));
}