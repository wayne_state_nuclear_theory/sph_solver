//  file: SPH_Particle.cpp
//
//  Definitions for the SPH_Particle C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history
//      05/29/19  Original version
//      05/29/19  Made a typedef fourvec and added a main function to test
//      05/30/19  Updated to not use namespace std, added position_ 
//      05/30/19  Split into separate header and definition files
//      05/30/19  Add checks for different cases
//
//  Notes:
//
//***************************************************************************

//include files
#include "SPH_Particle.h"  // include the header for this class
#include "constant.h"
#include <iostream>
#include <cmath>

using Utility::abserr;
using Utility::small_eps;

//***************************************************************************

// Constructor
SPH_Particle::SPH_Particle(const myreal mass, const myreal rho,
    const std::vector<myreal> &weight, const fourvec &x, const fourvec &u, 
    const myreal h, const int dim) {
    // This function constructs a particle given mass, density (rho), weight, 
    // position x, velocity u, and width h of W

    mass_ = mass;
    rho_ = rho;  // density
    weight_ = weight;  // a vector of some sort of weights
    position_ = x;  // position fourvector
    velocity_ = u;  // velocity fourvector
    h_ = h; // width of smoothing function W
    radius_sq_ = 1.; // the length of support of W squared
    dim_ = dim; // dimension of the particle

    // check if mass < 0
    if (mass_ < 0.) {
        std::cout << "ERROR: Mass is negative. mass = " << mass_ << std::endl;
        exit(0);
    }

    // check if t < 0
    if (position_[0] < 0.) {
        std::cout << "ERROR: Time is negative. time = " << position_[0]
                  << std::endl;
        exit(0);
    }

    // check if h < 0
    if (h_ < 0.) {
        std::cout << "ERROR: h is negative. h = " << h_
                  << std::endl;
        exit(0);
    }

    // check if dim < 1 or dim > 3
    if ((dim_ < 1)||(dim_ > 3)) {
        std::cout << "ERROR: dim is not 1, 2, or 3. dim = " << dim_
                  << std::endl;
        exit(0);
    }

    // check that norm = 1
    auto u_good = check_velocity_norm(u);
    if (!u_good) {
        exit(0);
    }
}


// Defualt constructor
SPH_Particle::SPH_Particle() {
    // This function is the default constructor for an SPH_Particle, assigning 
    // default values to the private variables
    mass_ = 1.;
    rho_ = 1.;
    position_ = {0., 0., 0., 0.};
    velocity_ = {1., 0., 0., 0.};
    h_ = 0.1;
    radius_sq_ = 1.;
    dim_ = 3;
}


SPH_Particle::~SPH_Particle() { // Desctructor
    // This function is the desctructor
    // std::cout << "SPH_Particle Destructor called" << std::endl;
}


myreal SPH_Particle::W(const fourvec &x) const {
    // This function computes the smoothing function W at x - position_, where 
    // x is any arbitrary position.
    return(0.);
}


myreal SPH_Particle::dr_W(const fourvec &x) const {
    // This function computes dr_W for the smoothing function at x - position_,
    // where x is any arbitrary position.
    return(0.);
}


myreal SPH_Particle::dr2_W(const fourvec &x) const {
    // This function computes d^2/dr^2(W) for the smoothing function 
    // at x - position_, where x is any arbitrary position.
    return(0.);
}


myreal SPH_Particle::dx_W(const fourvec &x) const {
    // This function computes the smoothing function dx_W at x - position_,
    // where x is any arbitrary position. This derivative is with respect
    // to the passed variable's x.
    fourvec diff = x - position_;
    myreal dist = diff.three_norm();
    return (diff[1])/(dist + small_eps) * dr_W(x);
}


myreal SPH_Particle::dy_W(const fourvec &x) const {
    // This function computes the smoothing function dy_W at x - position_,
    // where x is any arbitrary position. This derivative is with respect
    // to the passed variable's y.
    fourvec diff = x - position_;
    myreal dist = diff.three_norm();
    return (diff[2])/(dist + small_eps) * dr_W(x);
}


myreal SPH_Particle::dz_W(const fourvec &x) const {
    // This function computes the smoothing function dz_W at x - position_,
    // where x is any arbitrary position. This derivative is with respect
    // to the passed variable's z.
    fourvec diff = x - position_;
    myreal dist = diff.three_norm();
    return (diff[3])/(dist + small_eps) * dr_W(x);
}


myreal SPH_Particle::dx2_W(const fourvec &x) const {
    // This function computes the smoothing function d^2/dx^2(W) 
    // at x - position_, where x is any arbitrary position. This derivative 
    // is with respect to the passed variable's x.
    fourvec diff = x - position_;
    myreal dist = diff.three_norm();
    return (diff[1])/(dist + small_eps)*(diff[1])/(dist + small_eps) * dr2_W(x)
        + (diff[2]*diff[2] + diff[3]*diff[3])/(dist*dist*dist + small_eps) 
        * dr_W(x);
}


myreal SPH_Particle::dy2_W(const fourvec &x) const {
    // This function computes the smoothing function d^2/dy^2(W) 
    // at x - position_, where x is any arbitrary position. This derivative 
    // is with respect to the passed variable's y.
    fourvec diff = x - position_;
    myreal dist = diff.three_norm();
    return (diff[2])/(dist + small_eps)*(diff[2])/(dist + small_eps) * dr2_W(x)
        + (diff[1]*diff[1] + diff[3]*diff[3])/(dist*dist*dist + small_eps) 
        * dr_W(x);
}


myreal SPH_Particle::dz2_W(const fourvec &x) const {
    // This function computes the smoothing function d^2/dz^2(W) 
    // at x - position_, where x is any arbitrary position. This derivative 
    // is with respect to the passed variable's z.
    fourvec diff = x - position_;
    myreal dist = diff.three_norm();
    return (diff[3])/(dist + small_eps)*(diff[3])/(dist + small_eps) * dr2_W(x)
        + (diff[1]*diff[1] + diff[2]*diff[2])/(dist*dist*dist + small_eps) 
        * dr_W(x);
}


void SPH_Particle::set_mass(const myreal mass) {
    // This function sets the mass_ of the particle to a given mass

    // check if mass < 0
    if (mass < 0.) {
        std::cout << "ERROR: Mass is negative. mass = " << mass << std::endl;
        exit(0);
    }
    mass_ = mass;
}


void SPH_Particle::set_rho(const myreal rho) {
    // This function sets the density rho_ of the particle to a given rho
    rho_ = rho;
}


void SPH_Particle::set_weight(const std::vector<myreal> &weight) {
    // This function sets the weight_ of the particle to a given weight vector

    // check that the new weight vector has the right size
    if ((weight_.size() != 0) && (weight_.size() != weight.size())) {
        std::cout << "ERROR: Weight vector has wrong size. "
                  << "Size of weight vector is " << weight.size() << ". "
                  << "Size of weight vector should be " << weight_.size()
                  << "." << std::endl;
        exit(0);
    }
    weight_ = weight;
}


void SPH_Particle::set_position(const fourvec &x) {
    // This function sets the position_ of the particle to a given x

    // check if t < 0
    if (x[0] < 0.) {
        std::cout << "ERROR: Time is negative. time = " << x[0]
                  << std::endl;
        exit(0);
    }
    position_ = x;
}


void SPH_Particle::set_velocity(const fourvec &u) {
    // This function sets the velocity_ of the particle to a given u

    // check that the given u has norm 1
    auto u_good = check_velocity_norm(u);
    if (!u_good) {
        exit(0);
    }
    velocity_ = u;
}


void SPH_Particle::set_h_int(const myreal h) {
    // This function sets the width h_ of the smooothing function to a given h.
    // This function is internal, so that set_h can be modified for derived 
    // classes.

    // check if h < 0
    if (h < 0.) {
        std::cout << "ERROR: h is negative. h = " << h
                  << std::endl;
        exit(0);
    }
    h_ = h;
}


void SPH_Particle::set_h(const myreal h) {
    // This function sets the width h_ of the smooothing function to a given h
    set_h_int(h);
}


void SPH_Particle::set_radius_sq(const myreal radius_sq) {
    // This function sets the radius_sq_ of the smooothing function to a given 
    // radius_sq

    // check if radius_sq < 0
    if (radius_sq < 0.) {
        std::cout << "ERROR: radius_sq is negative. radius_sq = " << radius_sq
                  << std::endl;
        exit(0);
    }
    radius_sq_ = radius_sq;
}


void SPH_Particle::set_dim_int(const int dim) {
    // This function sets the dimension of the particle. This function is 
    // internal, so that set_dim can be modified for derived classes.

    // check if dim < 1 or dim > 3
    if ((dim < 1)||(dim > 3)) {
        std::cout << "ERROR: dim is not 1, 2, or 3. dim = " << dim_
                  << std::endl;
        exit(0);
    }
    dim_ = dim;
}


void SPH_Particle::set_dim(const int dim) {
    // This function sets the dimension of the particle.
    set_dim_int(dim);
}


void SPH_Particle::add_to_neighbor_list(
    const std::shared_ptr<SPH_Particle> particle_ptr) {
    // This function adds a pointer to another particle to the neighbor_list_ 
    // of this particle
    std::weak_ptr<SPH_Particle> temp_ptr = particle_ptr;
    neighbor_list_.push_back(temp_ptr);
}

    
bool SPH_Particle::check_velocity_norm(const fourvec &u) const {
    // This function checks the norm of velocity vector, u^\mu u_\mu = 1
    bool pass = true;
    
    if (std::abs(u.norm() - 1.) > u[0]*abserr) {
        pass = false;
        std::cout << "ERROR: norm of velocity is not equal to 1. norm = "
                  << u.norm() << std::endl;
    }
    return(pass);
}


bool SPH_Particle::check_distance(const fourvec &x) const {
    // This function checks the distance of this particle to another particle 
    // at position x
    bool pass = true;
    fourvec diff = position_ - x;
    myreal dist_sq = diff.three_norm_sq();

    if (dist_sq > radius_sq_ + 1.e6 * abserr) {
        pass = false;
    }
    return(pass);
}
