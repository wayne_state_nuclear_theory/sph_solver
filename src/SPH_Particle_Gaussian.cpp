//  file: SPH_Particle_Gaussian.cpp
//
//  Definitions for the SPH_Particle_Gaussian C++ derived class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history
//      06/17/19  Original file
//
//  Notes:
//
//***************************************************************************

//include files
#include "SPH_Particle_Gaussian.h"  // include the header for this class
#include "constant.h"
#include <iostream>
#include <cmath>

using Utility::abserr;
using Utility::small_eps;

//***************************************************************************

// Constructor
SPH_Particle_Gaussian::SPH_Particle_Gaussian(const myreal mass, 
	const myreal rho, const std::vector<myreal> &weight, const fourvec &x,
	const fourvec &u, const myreal h, const int dim) : kappa_(5.) {
	// This function constructs an SPH_Particle_Gaussian given mass, 
	// density (rho), weight, position x, velocity u, and width h of W. It also
	// sets the default value of kappa_ and sets radius_sq
	set_mass(mass);
	set_rho(rho);
	set_weight(weight);
	set_position(x);
	set_velocity(u);
	set_h(h);
    set_dim(dim);
}


// Defualt constructor
SPH_Particle_Gaussian::SPH_Particle_Gaussian() : kappa_(5.) {
    // This function is the default constructor for an SPH_Particle_Gaussian, 
    // assigning default values to the private variables and setting radius_sq
    myreal h_temp = get_h();
	set_radius_sq((kappa_*h_temp)*(kappa_*h_temp));
}

SPH_Particle_Gaussian::~SPH_Particle_Gaussian() { // Desctructor
    // This function is the desctructor
    // std::cout << "SPH_Particle_Gaussian Destructor called" << std::endl;
}


void SPH_Particle_Gaussian::set_h(const myreal h) {
    // This function sets the width h_ of the smooothing function to a given h
    set_h_int(h);
    set_radius_sq((h*kappa_)*(h*kappa_));

    // also reset the normalization constant
    myreal dim = get_dim();
    myreal h_sq = h*h;
    norm_const_ = 1./(pow(h_sq * 2. * M_PI, double(dim)/2.));
}


void SPH_Particle_Gaussian::set_dim(const int dim) {
    // This function sets the dimension of the particle.
    set_dim_int(dim);
    // also reset the normalization constant
    myreal h_temp = get_h();
    myreal h_sq = h_temp * h_temp;
    norm_const_ = 1./(pow(h_sq * 2. * M_PI, double(dim)/2.));
}


myreal SPH_Particle_Gaussian::W(const fourvec &x) const {
    // This function computes the Gaussian smoothing function W at 
    // x - position_, where x is any arbitrary position.
    fourvec position = get_position();
    myreal h = get_h();
    fourvec diff = x - position;
    myreal dist_sq = diff.three_norm_sq();
    return norm_const_ * exp(-dist_sq/(2. * h * h));
}


myreal SPH_Particle_Gaussian::dr_W(const fourvec &x) const {
    // This function computes dr_W for a Gaussian smoothing function at 
    // x - position_, where x is any arbitrary position.
    fourvec position = get_position();
    myreal h = get_h();
    myreal h_sq = h*h;
    fourvec diff = x - position;
    myreal dist_sq = diff.three_norm_sq();
    myreal dist = sqrt(dist_sq);
    return (-dist)/(h_sq) * norm_const_ * exp(-dist_sq/(2. * h_sq));
}


myreal SPH_Particle_Gaussian::dr2_W(const fourvec &x) const {
    // This function computes dr2_W for a Gaussian smoothing function at 
    // x - position_, where x is any arbitrary position.
    fourvec position = get_position();
    myreal h = get_h();
    myreal h_sq = h*h;
    fourvec diff = x - position;
    myreal dist_sq = diff.three_norm_sq();
    return ((-1.)/(h_sq) + (dist_sq)/(h_sq*h_sq)) 
        * norm_const_ * exp(-dist_sq/(2. * h_sq));
}


myreal SPH_Particle_Gaussian::dx2_W(const fourvec &x) const {
    // This function computes the smoothing function d^2/dx^2(W) 
    // at x - position_, where x is any arbitrary position. This derivative 
    // is with respect to the passed variable's x.
    fourvec position = get_position();
    myreal h = get_h();
    myreal h_sq = h*h;
    fourvec diff = x - position;
    myreal dist_sq = diff.three_norm_sq();
    return((-1.)/(h_sq) + (diff[1] * diff[1])/(h_sq * h_sq))
        * norm_const_ * exp(-dist_sq/(2. * h_sq));
}


myreal SPH_Particle_Gaussian::dy2_W(const fourvec &x) const {
    // This function computes the smoothing function d^2/dy^2(W) 
    // at x - position_, where x is any arbitrary position. This derivative 
    // is with respect to the passed variable's y.
    fourvec position = get_position();
    myreal h = get_h();
    myreal h_sq = h*h;
    fourvec diff = x - position;
    myreal dist_sq = diff.three_norm_sq();
    return((-1.)/(h_sq) + (diff[2] * diff[2])/(h_sq * h_sq))
        * norm_const_ * exp(-dist_sq/(2. * h_sq));
}


myreal SPH_Particle_Gaussian::dz2_W(const fourvec &x) const {
    // This function computes the smoothing function d^2/dz^2(W) 
    // at x - position_, where x is any arbitrary position. This derivative 
    // is with respect to the passed variable's z.
    fourvec position = get_position();
    myreal h = get_h();
    myreal h_sq = h*h;
    fourvec diff = x - position;
    myreal dist_sq = diff.three_norm_sq();
    return((-1.)/(h_sq) + (diff[3] * diff[3])/(h_sq * h_sq))
        * norm_const_ * exp(-dist_sq/(2. * h_sq));
}