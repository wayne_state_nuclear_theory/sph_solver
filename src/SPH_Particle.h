//  file: SPH_Particle.h
//
//  Header file for the SPH_Particle C++ class.
//
//  Programmer:  Michael Heinz  heinz.38@osu.edu
//
//  Revision history:
//      05/30/19  Original header file
//
//  Notes:
//
//***************************************************************************

// The ifndef/define macro ensures that the header is only included once
#ifndef SPH_PARTICLE_H
#define SPH_PARTICLE_H

// include files
#include <vector>
#include <memory>
#include "datastruct.h"
#include "fourvec.h"

class SPH_Particle { 
    private:
        myreal mass_;
        myreal rho_;  // density
        std::vector<myreal> weight_; // a vector of some sort of weights
        fourvec position_;  // position fourvector
        fourvec velocity_;  // velocity fourvector 
        myreal h_;  // width of smoothing function W
        myreal radius_sq_;  // the length of support of W squared
        int dim_; // dimension that the particle is in
        // list of neighboring particles
        std::vector<std::weak_ptr<SPH_Particle>> neighbor_list_;

    public:
        SPH_Particle(const myreal mass, const myreal rho, 
                     const std::vector<myreal> &weight, 
                     const fourvec &x, const fourvec &u,
                     const myreal h, const int dim);  // Constructor
        SPH_Particle(); // Default Constructor
        virtual ~SPH_Particle();  // Destructor

        // smoothing functions and derivatives
        virtual myreal W(const fourvec &x) const;
        virtual myreal dr_W(const fourvec &x) const;
        virtual myreal dr2_W(const fourvec &x) const;
        myreal dx_W(const fourvec &x) const;
        myreal dy_W(const fourvec &x) const;
        myreal dz_W(const fourvec &x) const;
        virtual myreal dx2_W(const fourvec &x) const;
        virtual myreal dy2_W(const fourvec &x) const;
        virtual myreal dz2_W(const fourvec &x) const;
    
        // accessor functions
        void set_mass(const myreal mass);
        void set_rho(const myreal rho);
        void set_weight(const std::vector<myreal> &weight);
        void set_position(const fourvec &x);
        void set_velocity(const fourvec &u);
        void set_h_int(const myreal h);
        virtual void set_h(const myreal h);
        void set_radius_sq(const myreal radius_sq);
        void set_dim_int(const int dim);
        virtual void set_dim(const int dim);
        void add_to_neighbor_list(
            const std::shared_ptr<SPH_Particle> particle_ptr);
        void clear_neighbor_list() {neighbor_list_.clear();}
    
        myreal get_mass() const {return(mass_);}
        myreal get_rho() const {return(rho_);}
        std::vector<myreal> get_weight() const {return(weight_);}
        fourvec get_position() const {return(position_);}
        fourvec get_velocity() const {return(velocity_);}
        myreal get_h() const {return(h_);}
        int get_dim() const {return(dim_);}
        std::vector<std::weak_ptr<SPH_Particle>> get_neighbor_list() const {
            return(neighbor_list_);
        }
 
        //! This function checks the normalization of velocity vector,
        //! u^\mu u_\mu = 1
        bool check_velocity_norm(const fourvec &u) const;

        //! This function checks the distance of this particle to another
        //! particle at position x2
        bool check_distance(const fourvec &x2) const;
};

#endif  // SPH_Particle_H
